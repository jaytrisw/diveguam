//
//  ConditionsViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 7/3/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import YXWaveView
import FloatingPanel

class ConditionsViewController: UIViewController {
    
    
    fileprivate var waterView: YXWaveView?
    var fpc: FloatingPanelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Init Avatar OverView
        let avatarFrame = CGRect(x: 0, y: 0, width: 136, height: 120)
        let avatarView = UIImageView(frame: avatarFrame)
        //avatarView.layer.cornerRadius = avatarView.bounds.height/2
        avatarView.layer.masksToBounds = true
        //avatarView.layer.borderColor  = UIColor.white.cgColor
        //avatarView.layer.borderWidth = 3
        avatarView.layer.contents = UIImage(named: "dive-flag")?.cgImage
        
        let frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height/3)
        waterView = YXWaveView(frame: frame, color: UIColor(red: 0/255, green: 0/255, blue: 111/255, alpha: 1))
        waterView!.setGradientBackground(colorOne: Colors.deepSkyBlue, colorTwo: Colors.skyBlue)
        waterView!.addOverView(avatarView)
        waterView!.backgroundColor = UIColor(red: 135/255, green: 206/255, blue: 255/255, alpha: 1)
        waterView?.waveCurvature = 0.5
        waterView?.waveSpeed = 0.4
        waterView?.waveHeight = 10
        
        // Add WaveView
        self.view.addSubview(waterView!)
        
        // Start wave
        waterView!.start()
        
        let button = UIButton()
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            button.frame = CGRect(x: 4, y: 44, width: 44, height: 44)
        } else {
            button.frame = CGRect(x: 3, y: 20, width: 44, height: 44)
        }
        button.setImage(UIImage(named: "close"), for: .normal)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        waterView?.addSubview(button)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func closeAction(sender: UIButton!) {
        //dismiss(animated: true, completion: nil)
        fpc.removePanelFromParent(animated: true, completion: nil)
        
    }
    
}

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor: UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.35 { didSet { updateLocations() }}
    @IBInspectable var endLocation: Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode: Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode: Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
