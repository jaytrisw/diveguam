//
//  OpenItemSpecialViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/12/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Toast_Swift

class OpenFirstAidItemViewController: UIViewController, UIScrollViewDelegate {
        
    // MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentView: UITextView!
    @IBOutlet weak var symptomsView: UITextView!
    @IBOutlet weak var descriptionView: UITextView!
        
    // MARK: - Arrays
    var SingleItem:ItemNews?
    
    // MARK: - Variables
    var defaultImage:UIImage!
    var firstAidContent:String?
    var firstAidSymptoms:String?
    var firstAidDescription:String?
    var firstAidTitle:String?
    var firstAidImageUrl:String?
    var liteVersion:Bool = false
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
        
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        scrollView.delegate = self
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        putData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        if offset < 0 {
            let headerScaleFactor:CGFloat = -(offset) / imageView.bounds.height
            let headerSizevariation = ((imageView.bounds.height * (1.0 + headerScaleFactor)) - imageView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            imageView.layer.transform = headerTransform
        } else {
        }
        imageView.layer.transform = headerTransform
    }
    
    // MARK: - Controller Functions
    func putData(){
        defaultImage = UIImage(named:"placeholder")!
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        var esc_str:String?
        if liteVersion { esc_str = firstAidImageUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        } else { esc_str = SingleItem?.imgUrl!
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        }
        if(esc_str == nil || esc_str == ""){
            imageView.image = defaultImage
        } else {
            imageView.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        if self.liteVersion {
            titleLabel.text = firstAidTitle
            contentView.text = firstAidContent
            symptomsView.text = firstAidSymptoms
            descriptionView.text = firstAidDescription
        } else {
            titleLabel.text = SingleItem?.title
            contentView.text = SingleItem?.newContent
            symptomsView.text = SingleItem?.firstAidSymptoms
            descriptionView.text = SingleItem?.firstAidDescription
        }
    }

    // MARK: - Actions
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
