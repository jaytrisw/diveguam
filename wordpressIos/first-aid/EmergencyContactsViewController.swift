//
//  EmergencyContactsViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/13/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import MapKit

let offset_HeaderStop:CGFloat = 50.0
let offset_AvatarStop:CGFloat = 0.0

class EmergencyContactsViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var header: UIVisualEffectView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chamberNumber: UIButton!
    @IBAction func chamberNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = chamberNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var chamberNumberAlt: UIButton!
    @IBAction func chamberNumberAlt(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = chamberNumberAlt.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var uscgNumber: UIButton!
    @IBAction func uscgNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = uscgNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var usnhNumber: UIButton!
    @IBAction func usnhNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = usnhNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var gmhNumber: UIButton!
    @IBAction func gmhNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = gmhNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var grmcNumber: UIButton!
    @IBAction func grmcNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = grmcNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var call911: UIButton!
    @IBAction func call911(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = call911.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    @IBOutlet weak var navyEmergencyNumber: UIButton!
    @IBAction func navyEmergencyNumber(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted
        let number = navyEmergencyNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        callNumber(phoneNumber: cleanChars!)
    }
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func ghmMap(_ sender: Any) {
        // 13.504499, 144.774936
        openMaps(latitude: 13.504499, longitude: 144.774936, title: "Guam Memorial Hospital")
    }
    @IBAction func usnhMap(_ sender: Any) {
        // 13.474825, 144.737351
        openMaps(latitude: 13.474825, longitude: 144.737351, title: "U.S. Naval Hospital")
    }
    @IBAction func grmcMap(_ sender: Any) {
        openMaps(latitude: 13.525592, longitude: 144.823029, title: "Guam Regional Medical City")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        scrollView.delegate = self
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            header.layer.transform = headerTransform
        } else {
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
        }
        header.layer.transform = headerTransform
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func callNumber(phoneNumber: String)  {
        let url: NSURL = URL(string: "TEL://\(phoneNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    func openMaps(latitude: Double, longitude: Double, title: String) {
        let latitude:CLLocationDegrees = latitude
        let longitude:CLLocationDegrees = longitude
        let regionDistance:CLLocationDistance = 500;
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(title)"
        mapItem.openInMaps(launchOptions: options)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
