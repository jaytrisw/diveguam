//
//  DatabaseManager.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/16/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import SQLite

class DBHandler {
    
    var database: Connection!
    
    let myTable = Table("news")
    let settingTable = Table("settings")
    
    let new_id = Expression<Int64>("new_id")
    let new_post_id = Expression<String>("new_post_id")
    let new_title = Expression<String>("new_title")
    let new_img_url = Expression<String>("new_img_url")
    let new_datetime = Expression<String>("new_datetime")
    let new_writer = Expression<String>("new_writer")
    let new_url = Expression<String>("new_url")
    let new_content = Expression<String>("new_content")
    let new_depth_range = Expression<String>("new_depth_range")
    let new_vis_range = Expression<String>("new_vis_range")
    let new_latitude = Expression<String>("new_latitude")
    let new_longitude = Expression<String>("new_longitude")
    let new_city = Expression<String>("new_city")
    let new_cert = Expression<String>("new_cert")
    let new_access = Expression<String>("new_access")
    let new_hazards = Expression<String>("new_hazards")
    let new_site_map = Expression<String>("new_site_map")
    let new_comment_count = Expression<String>("new_comment_count")
    let new_marine_life = Expression<String>("new_marine_life")
    let new_gallery = Expression<String>("new_gallery")
    let new_warning = Expression<String>("new_warning")
    let new_warning_report_contact = Expression<String>("new_warning_report_contact")
    let new_warning_report_label = Expression<String>("new_warning_report_label")
    let new_warning_subject = Expression<String>("new_warning_subject")
    let new_warning_instructions = Expression<String>("new_warning_instructions")
    let new_first_aid = Expression<String>("new_first_aid")
    let new_first_aid_title = Expression<String>("new_first_aid_title")
    let new_first_aid_content = Expression<String>("new_first_aid_content")
    let new_first_aid_image_URL = Expression<String>("new_first_aid_image_URL")
    let new_first_aid_symptoms = Expression<String>("new_first_aid_symptoms")
    let new_first_aid_description = Expression<String>("new_first_aid_description")
    let new_avatar = Expression<String>("new_avatar")
    let new_author = Expression<String>("new_author")
    let new_authorProRating = Expression<String>("new_authorProRating")
    let new_authorProNumber = Expression<String>("new_authorProNumber")
    let new_web_id = Expression<String>("new_web_id")
    
    let setting_item = Expression<String>("setting_item")
    let setting_value = Expression<Int>("setting_value")


    
    func beforeStart() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("news").appendingPathExtension("sqlite3")
            let db = try Connection(fileUrl.path)
            self.database = db
            createTable()
        } catch {
          print(error)
        }
    }
    
    func createTable() {
        let createTable = self.myTable.create { (table) in
            table.column(new_id , primaryKey: .autoincrement)
            table.column(new_post_id)
            table.column(new_title)
            table.column(new_img_url)
            table.column(new_datetime)
            table.column(new_writer)
            table.column(new_url)
            table.column(new_content)
            table.column(new_depth_range)
            table.column(new_vis_range)
            table.column(new_latitude)
            table.column(new_longitude)
            table.column(new_city)
            table.column(new_cert)
            table.column(new_access)
            table.column(new_hazards)
            table.column(new_site_map)
            table.column(new_comment_count)
            table.column(new_marine_life)
            table.column(new_gallery)
            table.column(new_warning)
            table.column(new_warning_report_contact)
            table.column(new_warning_report_label)
            table.column(new_warning_subject)
            table.column(new_warning_instructions)
            table.column(new_first_aid)
            table.column(new_first_aid_title)
            table.column(new_first_aid_content)
            table.column(new_first_aid_image_URL)
            table.column(new_first_aid_symptoms)
            table.column(new_first_aid_description)
            table.column(new_avatar)
            table.column(new_author)
            table.column(new_authorProRating)
            table.column(new_authorProNumber)
            table.column(new_web_id, unique: true)
        }
        
        do{
            try self.database.run(createTable)
            print("table created")
        }catch{
            print(error)
        }
    }
    func insertNews(item:ItemNews) -> Bool {
        let insert = myTable.insert(new_post_id <- item.postID!,
                                  new_title <- item.title!,
                                  new_img_url <- item.imgUrl!,
                                  new_datetime <- item.datetime!,
                                  new_writer <- item.writer!,
                                  new_url <- item.newUrl!,
                                  new_content <- item.newContent!,
                                  new_content <- item.newContent!,
                                  new_depth_range <- item.visRange!,
                                  new_vis_range <- item.depthRange!,
                                  new_latitude <- item.latitude!,
                                  new_longitude <- item.longitude!,
                                  new_city <- item.city!,
                                  new_cert <- item.certificationLevel!,
                                  new_access <- item.access!,
                                  new_hazards <- item.hazards!,
                                  new_site_map <- item.siteMap!,
                                  new_comment_count <- item.commentCount!,
                                  new_marine_life <- item.marineLife!,
                                  new_gallery <- item.gallery!,
                                  new_warning <- item.warning!,
                                  new_warning_report_contact <- item.warningContact!,
                                  new_warning_report_label <- item.warningLabel!,
                                  new_warning_subject <- item.warningSubject!,
                                  new_warning_instructions <- item.warningInstructions!,
                                  new_first_aid <- item.firstAid!,
                                  new_first_aid_title <- item.firstAidTitle!,
                                  new_first_aid_content <- item.firstAidContent!,
                                  new_first_aid_image_URL <- item.firstAidImageUrl!,
                                  new_first_aid_symptoms <- item.firstAidSymptoms!,
                                  new_first_aid_description <- item.firstAidDescription!,
                                  new_avatar <- item.avatar!,
                                  new_author <- item.author!,
                                  new_authorProRating <- item.authorProRating!,
                                  new_authorProNumber <- item.authorProNumber!,
                                  new_web_id <- item.id!)
        do{
            try self.database.run(insert)
            print("item inserted")
            return true
        }catch{
            print(error)
            return false
        }
    }
    func getNews(lastId:Int) -> Array<ItemNews> {
        var NewsList = Array<ItemNews>()
        do{
            if lastId == 0 {
                let query = self.myTable
                    .order(self.new_id.desc)
                    .limit(10)
                let news = try self.database.prepare(query)
                for new in news {
                    let item = ItemNews(
                        id: "\(new[self.new_id])",
                        postID: new[self.new_post_id],
                        title: new[self.new_title],
                        imgUrl: new[self.new_img_url] ,
                        datetime: new[self.new_datetime],
                        writer: new[self.new_writer],
                        newUrl: new[self.new_url],
                        newContent:new[self.new_content],
                        depthRange:new[self.new_depth_range],
                        visRange:new[self.new_vis_range],
                        latitude:new[self.new_latitude],
                        longitude:new[self.new_longitude],
                        city:new[self.new_city],
                        certificationLevel:new[self.new_cert],
                        access:new[self.new_access],
                        hazards:new[self.new_hazards],
                        siteMap:new[self.new_site_map],
                        commentCount:new[self.new_comment_count],
                        marineLife:new[self.new_marine_life],
                        gallery:new[self.new_gallery],
                        warning:new[self.new_warning],
                        warningContact:new[self.new_warning_report_contact],
                        warningLabel:new[self.new_warning_report_label],
                        warningSubject:new[self.new_warning_subject],
                        warningInstructions:new[self.new_warning_instructions],
                        firstAid:new[self.new_first_aid],
                        firstAidTitle:new[self.new_first_aid_title],
                        firstAidContent:new[self.new_first_aid_content],
                        firstAidImageUrl:new[self.new_first_aid_image_URL],
                        firstAidSymptoms:new[self.new_first_aid_symptoms],
                        firstAidDescription:new[self.new_first_aid_description],
                        avatar:new[self.new_avatar],
                        author:new[self.new_author],
                        authorProRating:new[self.new_authorProRating],
                        authorProNumber:new[self.new_authorProNumber]
                    )
                    item.newWebId = new[self.new_web_id]
                    NewsList.append(item)
                }
            }else{
                let query = self.myTable
                    .order(self.new_id.desc)
                    .filter(self.new_id < Int64(lastId))
                    .limit(10)
                let news = try self.database.prepare(query)
                for new in news {
                    let item = ItemNews(
                        id: "\(new[self.new_id])",
                        postID: new[self.new_post_id],
                        title: new[self.new_title],
                        imgUrl: new[self.new_img_url] ,
                        datetime: new[self.new_datetime],
                        writer: new[self.new_writer],
                        newUrl: new[self.new_url],
                        newContent:new[self.new_content],
                        depthRange:new[self.new_depth_range],
                        visRange:new[self.new_vis_range],
                        latitude:new[self.new_latitude],
                        longitude:new[self.new_longitude],
                        city:new[self.new_city],
                        certificationLevel:new[self.new_cert],
                        access:new[self.new_access],
                        hazards:new[self.new_hazards],
                        siteMap:new[self.new_site_map],
                        commentCount:new[self.new_comment_count],
                        marineLife:new[self.new_marine_life],
                        gallery:new[self.new_gallery],
                        warning:new[self.new_warning],
                        warningContact:new[self.new_warning_report_contact],
                        warningLabel:new[self.new_warning_report_label],
                        warningSubject:new[self.new_warning_subject],
                        warningInstructions:new[self.new_warning_instructions],
                        firstAid:new[self.new_first_aid],
                        firstAidTitle:new[self.new_first_aid_title],
                        firstAidContent:new[self.new_first_aid_content],
                        firstAidImageUrl:new[self.new_first_aid_image_URL],
                        firstAidSymptoms:new[self.new_first_aid_symptoms],
                        firstAidDescription:new[self.new_first_aid_description],
                        avatar:new[self.new_avatar],
                        author:new[self.new_author],
                        authorProRating:new[self.new_authorProRating],
                        authorProNumber:new[self.new_authorProNumber]
                    )
                    item.newWebId = new[self.new_web_id]
                    NewsList.append(item)
                }
            }
            return NewsList
        }catch{
            
        }
        return NewsList
    }
    func deleteNews(item:ItemNews) -> Bool {
        let number: Int64? = Int64(item.id!)
        do{
            let selected = myTable.filter(new_id == number!)
            try database.run(selected.delete())
            print("item deleted")
            return true
        }catch{
            print(error)
            return false
        }
    }
    // notification settings part -- not implimented yet 
    func beforeStartSettings() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("news").appendingPathExtension("sqlite3")
            let db = try Connection(fileUrl.path)
            self.database = db
            createTableSetting()
        } catch {
            print(error)
        }
    }
    
    func createTableSetting() {
        let createTable = self.settingTable.create { (table) in
            table.column(setting_item )
            table.column(setting_value)
        }
        let insert = settingTable.insert(setting_item <- "notification",
                                    setting_value <- 1)
        do{
            try self.database.run(createTable)
            try self.database.run(insert)
            print("table setting created")
        }catch{
            print(error)
        }
    }
    func checkNotiSetting( completion:@escaping (_ value:Int)->Void){
        do {
            let selected = settingTable.filter(setting_item == "notification")
            let values = try self.database.prepare(selected)
            for value in values {
                completion(value[self.setting_value])
            }
            
        } catch {
            completion(1)
            print(error)
        }
        completion(1)
    }
    func updatNotiValue(newval:Int){
        do{
            let sett = settingTable.filter(setting_item == "notification")
            try self.database.run(sett.update(setting_value <- newval))
        }catch{
            print(error)
        }
    }
}
