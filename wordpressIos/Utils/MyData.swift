//
//  MyData.swift
//  wordpressIos
//
//  Created by mac on 1/13/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
class MyData{
    // app name
    public static var appName:String = "Guam Dive Guide"
    
    public static var apiKey:String = "&wpapikey=b34b40ca8771c48c204e55f927376885"
    // website address : be careful to add /?json= at the end
    public static var wpLink:String = "https://www.guamdiveguide.com?json="
    // notification link : only the link of the website with / at the end
    public static var wpNotifLink:String = "https://www.guamdiveguide.com/"

    // categories pages title
    public static var wpCategoryAllName:String = "Popular"
    public static var wpCategoryOneName:String = "Apra Harbor"
    public static var wpCategoryTwoName:String = "Southern Sites"
    public static var wpCategoryThreeName:String = "Northern Sites"
    
    // categories ids
    public static var wpCategoryOneID:String = "4"
    public static var wpCategoryTwoID:String = "7"
    public static var wpCategoryThreeID:String = "6"
    
    // addmob IDs
    public static var Ads_appID:String = "ca-app-pub-4886459340627712~7607914942"
    public static var Ads_interstitialID:String = "ca-app-pub-3940256099942544/4411468910"
    
    public static var numberOfClicks:Int = 0
}
