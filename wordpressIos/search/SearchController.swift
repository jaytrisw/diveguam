//
//  SearchController.swift
//  wordpressIos
//
//  Created by mac on 1/13/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class SearchController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var trendingSearch: UIStackView!
    @IBOutlet weak var trendingTwoButton: UIButton!
    @IBOutlet weak var trendingOneButton: UIButton!
    @IBAction func trendingTwoAction(_ sender: Any) {
        searchField.text = trendingTwoButton.currentTitle
        trendingSearch.alpha = 0
        putData()
    }
    @IBAction func trendingOneAction(_ sender: Any) {
        searchField.text = trendingOneButton.currentTitle
        trendingSearch.alpha = 0
        putData()
    }
    @IBOutlet weak var navBarTop: NSLayoutConstraint!
    @IBOutlet weak var toTop: NSLayoutConstraint!
    @IBOutlet weak var toNavBar: NSLayoutConstraint!
    @IBOutlet weak var buttonTrailing: NSLayoutConstraint!
    @IBAction func cancelAction(_ sender: Any) {
        self.buttonTrailing.constant = -60
        self.navBarTop.constant = 0
        navItem.title = "Search"
        toTop.isActive = false
        toNavBar.isActive = true
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        searchField.resignFirstResponder()
        page = 1
        NewsList.removeAll()
        CollectionViewList.reloadData()
        noConnectionLabel.alpha = 0
        trendingSearch.alpha = 0
        searchField.text = ""
    }
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewList: UICollectionView!
    @IBOutlet weak var noConnectionLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var searchContainer: UIVisualEffectView!
    
    @IBOutlet weak var collectionHeader: UICollectionReusableView!
    private let refreshControl = UIRefreshControl()
    var NewsList = Array<ItemNews>()
    var waiting:Bool = true
    var page:Int = 1
    var id:String=""
    var defaultImage:UIImage!
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBAction func refreshAction(_ sender: Any) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchField.addPadding(.both(10))
        searchField.delegate = self
        trendingSearch.alpha = 0
        loadingIndicator.stopAnimating()
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim, height: 75)
        defaultImage = UIImage(named:"placeholder")!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        self.buttonTrailing.constant = -60
        //self.navBarTop.constant = 0
        //toTop.isActive = false
        //toNavBar.isActive = true
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        page = 1
        NewsList.removeAll()
        CollectionViewList.reloadData()
        putData()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchField.text = ""
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.buttonTrailing.constant = 10
        self.navBarTop.constant = -65
        //navBar.alpha = 0
        navItem.title = ""
        toTop.isActive = true
        toNavBar.isActive = false
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        
    }
    
    func putData() {
        noConnectionLabel.alpha = 0
        CollectionViewList.reloadData()
        loadingIndicator.startAnimating()
        checkReachability()
        let searchString = searchField.text
        let searchTerm = searchString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        ApiNews.getSearchResults(searchTerm: searchTerm!, page: page, count: 100, completion: { (data:Array<ItemNews>, success:Bool) in
            if success {
                for item in data {
                    self.NewsList.append(item)
                }
                self.CollectionViewList.reloadData()
                self.waiting = false
                self.loadingIndicator.stopAnimating()
            } else {
                
            }
            if self.NewsList.count == 0 {
                print("no results")
                self.noConnectionLabel.text = "No results for '\(String(describing: searchString!))'"
                self.noConnectionLabel.alpha = 1
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenItemController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? DiveSiteViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenItemWarningViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenFirstAidItemViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NewsList[indexPath.row].newSaved = false
        if (NewsList[indexPath.row].warning == "1") {
            performSegue(withIdentifier: "OpenItemSpecial", sender: NewsList[indexPath.row])
        } else if (NewsList[indexPath.row].firstAid == "1") {
            performSegue(withIdentifier: "OpenFirstAideItem", sender: NewsList[indexPath.row])
        } else {
            performSegue(withIdentifier: "OpenSite", sender: NewsList[indexPath.row])
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.NewsList.count - 1 && !self.waiting  {  //numberofitem count
            page = page+1
            updateNextSet(page: page)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_search", for: indexPath) as! SearchCell
        //cell.dropShadow()
        cell.layimage.af_cancelImageRequest()
        cell.layimage.layer.removeAllAnimations()
        cell.layimage.image = nil
        cell.laytitle.text=NewsList[indexPath.row].title!
        cell.cityLabel.text=NewsList[indexPath.row].city!
        let esc_str = NewsList[indexPath.row].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        if(NewsList[indexPath.row].imgUrl == nil || NewsList[indexPath.row].imgUrl == ""){
            cell.layimage.image = defaultImage
        } else {
            if (NewsList[indexPath.row].warning != "1") {
                cell.layimage.af_setImage(
                    withURL: URL(string:esc_str!)!,
                    placeholderImage: defaultImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }
        }
        if (NewsList[indexPath.row].warning == "1") {
            cell.layimage.image = UIImage(named:"warning")!
            cell.backgroundColor = UIColor(red: 0.50, green: 0.0, blue: 0.0, alpha:1.0)
            cell.laytitle.textColor = UIColor.white
            cell.cityLabel.textColor = UIColor(red:0.69, green:0.30, blue:0.30, alpha:1.0)
        } else {
            cell.backgroundColor = UIColor.white
            cell.laytitle.textColor = UIColor.black
            cell.cityLabel.textColor = UIColor.darkGray
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func setData(id:String){
        self.id=id
    }
    func updateNextSet(page : Int ){
        self.waiting = true
        if(self.id.isEmpty){
            ApiNews.getNewsByAll(page: page, count: 75, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    if data.count > 0 {
                        self.waiting = false
                    }
                }
            })
        }else{
            ApiNews.getNewsByCategory(page: page, count: 75, id: self.id, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    if data.count > 0 {
                        self.waiting = false
                    }
                }
            })
        }
    }
    
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            self.CollectionViewList.reloadData()
            noConnectionLabel.text = "No internet connection."
            noConnectionLabel.alpha = 1
            refreshButton.alpha = 1
        }
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}
