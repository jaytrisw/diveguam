//
//  SearchCell.swift
//  wordpressIos
//
//  Created by Joshua Wood on 6/15/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {
    
    @IBOutlet weak var layimage: UIImageView!
    @IBOutlet weak var laytitle: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
}
