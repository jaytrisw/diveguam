//
//  FavoritesController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/25/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class FavoritesController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var NewsList = Array<ItemNews>()
    var id:String=""
    var defaultImage:UIImage!
    var  waiting:Bool = true
    var page:Int = 1
    let dbHandler = DBHandler()
    
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        putData()
        addRefresh()
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim, height: 75)
        defaultImage = UIImage(named:"placeholder")!
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(notification:)), name: NSNotification.Name(rawValue: "load"), object: nil)

    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
    
    @objc func loadList(notification: NSNotification) {
        updateData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NewsList[indexPath.row].newSaved = true
        if (NewsList[indexPath.row].warning == "1") {
            performSegue(withIdentifier: "OpenItemFaveSpecial", sender: NewsList[indexPath.row])
        } else if (NewsList[indexPath.row].firstAid == "1") {
            performSegue(withIdentifier: "OpenFirstAideItemFave", sender: NewsList[indexPath.row])
        } else {
            performSegue(withIdentifier: "OpenSiteFave", sender: NewsList[indexPath.row])
        }

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenItemController {
            if let News=sender as? ItemNews {
                dest.SingleItem=News
            }
        }
        if let dest = segue.destination as? DiveSiteViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenItemWarningViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem=News
            }
        }
        if let dest = segue.destination as? OpenFirstAidItemViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
    }
    func putData() {
        dbHandler.beforeStart()
        DispatchQueue.global().async {
            var data = Array<ItemNews>()
            data = self.dbHandler.getNews(lastId: 0)
            for item in data {
                self.NewsList.append(item)
            }
            DispatchQueue.main.async {
                self.CollectionViewList.reloadData()
                self.waiting = false
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("number of items fav", NewsList.count)
        return NewsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.NewsList.count - 1 && !self.waiting  {  //numberofitem count
            page = Int(NewsList[NewsList.count-1].id!)!
            updateNextSet(page: page)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FavoriteCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_one", for: indexPath) as! FavoriteCell
        // prepare
        cell.layimage.af_cancelImageRequest()
        cell.layimage.layer.removeAllAnimations()
        cell.layimage.image = nil
        // end prepare
        cell.laytitle.text=NewsList[indexPath.row].title!
        cell.cityLabel.text=NewsList[indexPath.row].city!
        let esc_str = NewsList[indexPath.row].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        if(NewsList[indexPath.row].imgUrl == nil || NewsList[indexPath.row].imgUrl == ""){
            cell.layimage.image = defaultImage
        }else{
            cell.layimage.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        return cell
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                print("Portrait")
            case .landscapeLeft,.landscapeRight :
                print("Landscape")
            default:
                print("Anything But Portrait")
            }
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            let dim = self.view.frame.size.width
            self.flowlayout.itemSize = CGSize(width: dim, height: 75)
        })
        super.viewWillTransition(to: size, with: coordinator)
    }
    func setData(id:String){
        self.id=id
    }
    func updateNextSet(page : Int ){
        self.waiting = true
        DispatchQueue.global().async {
            var data = Array<ItemNews>()
            data = self.dbHandler.getNews(lastId: page)
            for item in data {
                self.NewsList.append(item)
            }
            DispatchQueue.main.async {
                self.CollectionViewList.reloadData()
                if data.count > 0 {
                    self.waiting = false
                }
            }
        }
    }
    
    func addRefresh()  {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            CollectionViewList.refreshControl = refreshControl
        } else {
            CollectionViewList.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        // refreshControl.attributedTitle = NSAttributedString(string: "Downloading sites...")
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing dive sites...", attributes: attributes)

    }
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        updateData()
    }
    
    private func updateData() {
        self.NewsList.removeAll()
        self.CollectionViewList.reloadData()
        DispatchQueue.global().async {
            var data = Array<ItemNews>()
            data = self.dbHandler.getNews(lastId: 0)
            for item in data {
                self.NewsList.append(item)
            }
            DispatchQueue.main.async {
                self.CollectionViewList.reloadData()
                self.waiting = false
                self.refreshControl.endRefreshing()
            }
        }
    }
}



