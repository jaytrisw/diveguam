//
//  FavoriteCell.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/25/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class FavoriteCell: UICollectionViewCell {
    @IBOutlet weak var layimage: UIImageView!
    @IBOutlet weak var laytitle: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
}
