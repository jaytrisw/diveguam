//
//  CollectionViewCell.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 12/21/17.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var layimage: UIImageView!
    @IBOutlet weak var laytitle: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var laySection: UILabel!
    @IBOutlet weak var layDate: UILabel!
}
