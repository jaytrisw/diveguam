//
//  OpenItemWarningViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/12/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Toast_Swift
import MessageUI

let offset_ImageStop:CGFloat = 50.0

class OpenItemWarningViewController: UIViewController, MFMailComposeViewControllerDelegate, UIScrollViewDelegate {
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var SingleItem:ItemNews?
    var defaultImage:UIImage!
    @IBOutlet weak var instructionsView: UITextView!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentView: UITextView!
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func sendEmail(_ sender: Any) {
        sendEmail()
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is OpenFirstAidItemViewController {
            let vc = segue.destination as? OpenFirstAidItemViewController
            vc?.liteVersion = true
            vc?.firstAidContent = SingleItem?.firstAidContent
            vc?.firstAidSymptoms = SingleItem?.firstAidSymptoms
            vc?.firstAidDescription = SingleItem?.firstAidDescription
            vc?.firstAidTitle = SingleItem?.firstAidTitle
            vc?.firstAidImageUrl = SingleItem?.imgUrl
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        scrollView.delegate = self
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        putData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / imageView.bounds.height
            let headerSizevariation = ((imageView.bounds.height * (1.0 + headerScaleFactor)) - imageView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            imageView.layer.transform = headerTransform
        } else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_ImageStop, -offset), 0)
        }
        
        // Apply Transformations
        
        imageView.layer.transform = headerTransform
    }

    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([(SingleItem?.warningContact)!])
            mail.setSubject(String((SingleItem?.warningSubject)!))
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func putData(){
        if SingleItem?.warningContact == "error" {
            instructionsView.removeFromSuperview()
            emailButton.removeFromSuperview()
        }
        emailButton.setTitle(SingleItem?.warningLabel, for: .normal)
        instructionsView.text = SingleItem?.warningInstructions
        defaultImage = UIImage(named:"placeholder")!
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        let esc_str = SingleItem?.imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if(SingleItem?.imgUrl == nil || SingleItem?.imgUrl == ""){
            imageView.image = defaultImage
        } else {
            imageView.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        // navBarTitle.title = SingleItem?.title
        titleLabel.text = SingleItem?.title
        contentView.text = SingleItem?.newContent
        //textViewDidChange(contentView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
