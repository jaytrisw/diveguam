//
//  NewsController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/13/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class NewsController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - IBOutlets
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var noConnectionLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewList: UICollectionView!
    
    // MARK: - Arrays
    var NewsList = Array<ItemNews>()
    
    // MARK: - Variables
    var id:String=""
    var defaultImage:UIImage!
    var waiting:Bool = true
    var page:Int = 1
    private let refreshControl = UIRefreshControl()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
        addRefresh()
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim, height: 75)
        defaultImage = UIImage(named:"placeholder")!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenItemController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? DiveSiteViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenItemWarningViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenFirstAidItemViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
    }
    
    // MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //self.impactFeedbackGenerator.heavy.impactOccurred()
            NewsList[indexPath.row].newSaved = false
            if (NewsList[indexPath.row].warning == "1") {
                performSegue(withIdentifier: "OpenItemSpecial", sender: NewsList[indexPath.row])
            } else if (NewsList[indexPath.row].firstAid == "1") {
                performSegue(withIdentifier: "OpenFirstAideItem", sender: NewsList[indexPath.row])
            } else {
               performSegue(withIdentifier: "OpenSite", sender: NewsList[indexPath.row])
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewsList.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.NewsList.count - 1 && !self.waiting  {  //numberofitem count
            page = page+1
            updateNextSet(page: page)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_one", for: indexPath) as! CollectionViewCell
        //cell.dropShadow()
        cell.layimage.af_cancelImageRequest()
        cell.layimage.layer.removeAllAnimations()
        cell.layimage.image = nil
        cell.laytitle.text=NewsList[indexPath.row].title!
        cell.cityLabel.text=NewsList[indexPath.row].city!
        let esc_str = NewsList[indexPath.row].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        if(NewsList[indexPath.row].imgUrl == nil || NewsList[indexPath.row].imgUrl == ""){
            cell.layimage.image = defaultImage
        } else {
            if (NewsList[indexPath.row].warning != "1") {
                cell.layimage.af_setImage(
                    withURL: URL(string:esc_str!)!,
                    placeholderImage: defaultImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }
        }
        if (NewsList[indexPath.row].warning == "1") {
            cell.layimage.image = UIImage(named:"warning")!
            cell.backgroundColor = UIColor(red: 0.50, green: 0.0, blue: 0.0, alpha:1.0)
            cell.laytitle.textColor = UIColor.white
            cell.cityLabel.textColor = UIColor(red:0.69, green:0.30, blue:0.30, alpha:1.0)
        } else {
            cell.backgroundColor = UIColor.white
            cell.laytitle.textColor = UIColor.black
            cell.cityLabel.textColor = UIColor.darkGray
        }
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                print("Portrait")
            case .landscapeLeft,.landscapeRight :
                print("Landscape")
            default:
                print("Anything But Portrait")
            }
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            let dim = self.view.frame.size.width
            self.flowlayout.itemSize = CGSize(width: dim, height: dim / 1.5)
        })
        super.viewWillTransition(to: size, with: coordinator)
    }

    func putData() {
        CollectionViewList.reloadData()
        loadingIndicator.startAnimating()
        checkReachability()
        if(self.id.isEmpty){
            ApiNews.getNewsByAll(page: page, count: 75, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    self.waiting = false
                    self.loadingIndicator.stopAnimating()
                } else {
                    if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                        print("Failed to download JSON data")
                        self.loadingIndicator.stopAnimating()
                        self.refreshControl.endRefreshing()
                        self.CollectionViewList.reloadData()
                        self.noConnectionLabel.text = "Error parsing data"
                        self.noConnectionLabel.alpha = 1
                        self.refreshButton.alpha = 1
                    } else {
                        self.checkReachability()
                    }
                }
            })
        }else{
            ApiNews.getNewsByCategory(page: page, count: 75, id: self.id, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    self.waiting = false
                    self.loadingIndicator.stopAnimating()
                } else {
                    if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                        print("Failed to download JSON data")
                        self.loadingIndicator.stopAnimating()
                        self.refreshControl.endRefreshing()
                        self.CollectionViewList.reloadData()
                        self.noConnectionLabel.text = "Error parsing data"
                        self.noConnectionLabel.alpha = 1
                        self.refreshButton.alpha = 1
                    } else {
                        self.checkReachability()
                    }
                }
            })
        }
        self.refreshControl.endRefreshing()
    }
    func setData(id:String){
        self.id=id
    }
    func updateNextSet(page : Int ){
        self.waiting = true
        if(self.id.isEmpty){
            ApiNews.getNewsByAll(page: page, count: 75, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    if data.count > 0 {
                        self.waiting = false
                    }
                }
            })
        }else{
            ApiNews.getNewsByCategory(page: page, count: 75, id: self.id, completion: { (data:Array<ItemNews>, success:Bool) in
                if success {
                    for item in data {
                        self.NewsList.append(item)
                    }
                    self.CollectionViewList.reloadData()
                    if data.count > 0 {
                        self.waiting = false
                    }
                }
            })
        }
    }
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            self.CollectionViewList.reloadData()
            noConnectionLabel.text = "No internet connection."
            noConnectionLabel.alpha = 1
            refreshButton.alpha = 1
        }
    }
    func addRefresh()  {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            CollectionViewList.refreshControl = refreshControl
        } else {
            CollectionViewList.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        // refreshControl.attributedTitle = NSAttributedString(string: "Downloading sites...")
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing dive sites...", attributes: attributes)

        
    }
    
    // MARK: - Actions
    @objc private func refreshWeatherData(_ sender: Any) {
        checkReachability()
        page = 1
        self.NewsList.removeAll()
        self.CollectionViewList.reloadData()
        putData()
    }
    @IBAction func refreshAction(_ sender: Any) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
    }
}
