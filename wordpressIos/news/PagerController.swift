//
//  PagerController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/13/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import ACTabScrollView
class PagerController: UIViewController ,ACTabScrollViewDelegate, ACTabScrollViewDataSource{

    @IBOutlet weak var tabScrollView: ACTabScrollView!
    var contentViews: [UIView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // all the following properties are optional
        tabScrollView.defaultPage = 0
        tabScrollView.arrowIndicator = true
        tabScrollView.tabSectionBackgroundColor = UIColor(red: 190/255, green: 0/255, blue: 0/255, alpha: 1)
        tabScrollView.contentSectionBackgroundColor = UIColor.white
        
        tabScrollView.tabGradient = true
        tabScrollView.pagingEnabled = true
        tabScrollView.cachedPageLimit = 4
        
        //========
        tabScrollView.delegate = self
        tabScrollView.dataSource = self
        
        // create content views from storyboard
        
        
        getSectionData(id: "",all:true)
        getSectionData(id: MyData.wpCategoryOneID,all:false)
        getSectionData(id: MyData.wpCategoryTwoID,all:false)
        getSectionData(id: MyData.wpCategoryThreeID,all:false)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        print(index)
        
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
        
    }
    
    // MARK: ACTabScrollViewDataSource
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return contentViews.count
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        let label = UILabel()
        // put text
        switch index {
        case 0:
            label.text = String(describing: MyData.wpCategoryAllName)
        case 1:
            label.text = String(describing: MyData.wpCategoryOneName)
        case 2:
            label.text = String(describing: MyData.wpCategoryTwoName)
        case 3:
            label.text = String(describing: MyData.wpCategoryThreeName)
        default:
            label.text = String(describing: "name").uppercased()
        }
        
        if #available(iOS 8.2, *) {
            label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.thin)
        } else {
            label.font = UIFont.systemFont(ofSize: 16)
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        
        // if the size of your tab is not fixed, you can adjust the size by the following way.
        label.sizeToFit() // resize the label to the size of content
        label.frame.size = CGSize(width: label.frame.size.width + 28, height: label.frame.size.height + 36) // add some paddings
        
        return label
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    func getSectionData(id : String,all:Bool){
        if(!all){
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: "newsStory") as! NewsController
            vc.setData(id: id)
            /* set somethings for vc */
            addChild(vc) // don't forget, it's very important
            contentViews.append(vc.view)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: "newsStory") as! NewsController
            vc.setData(id: id)
            /* set somethings for vc */
            addChild(vc) // don't forget, it's very important
            contentViews.append(vc.view)
        }
       
    }
    

}
