//
//  SecondViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 12/21/17.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Toast_Swift

class SecondViewController: UIViewController {

    @IBOutlet weak var notiBtn: UIBarButtonItem!
    @IBOutlet weak var titleView: UINavigationItem!
    let dbHandler = DBHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // titleView.title=MyData.appName
       
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // notification setting part , not implimented yet ..
    @IBAction func openNotiSetting(_ sender: Any) {
        checkNotiSetting()
    }
    
    func checkNotiSetting(){
        
        dbHandler.beforeStartSettings()
        dbHandler.checkNotiSetting { (value) in
            print(value)
            self.showNotiSetting(notiValue: value)
        }
    }
    
    func showNotiSetting(notiValue:Int){
        var msg:String = ""
        var action = ""
        if notiValue == 0 {
            msg = " Push notification state is disabled"
            action = "Enable"
        }else{
            msg = " Push notification state is enabled "
            action = "Disable"
        }
       
        let alertController = UIAlertController(title: "Notifications setting", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let userAction = UIAlertAction(title: action, style: UIAlertAction.Style.destructive) {
            (result : UIAlertAction) -> Void in
            self.changeNotiValue(action: action)
        }
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
        }
        
        alertController.addAction(userAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func changeNotiValue(action:String){
        print(action)
        if action == "Enable" {
            dbHandler.updatNotiValue(newval: 1)
        }else{
            dbHandler.updatNotiValue(newval: 0)
        }
    }
    
}

