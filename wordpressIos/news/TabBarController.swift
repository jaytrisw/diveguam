import UIKit

class TabBarController: UITabBarController {
    
    @IBOutlet weak var bottomBar: UITabBar!
    
    enum tabBarMenu: Int {
        case home
        //case map
        case favorites
        case search
    }
    
    // MARK: UITabBarController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let tabBarMenuItem = tabBarMenu(rawValue: 0) else { return }
        setTintColor(forMenuItem: tabBarMenuItem)
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        guard
            let menuItemSelected = tabBar.items?.index(of: item),
            let tabBarMenuItem = tabBarMenu(rawValue: menuItemSelected)
            else { return }
        
        setTintColor(forMenuItem: tabBarMenuItem)
    }
    
    // MARK: Private
    private func setTintColor(forMenuItem tabBarMenuItem: tabBarMenu) {
        switch tabBarMenuItem {
        case .home:
            print("home")
            bottomBar.barTintColor = UIColor.black
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = UIColor.white
        //case .map:
            //print("favorites")
            //bottomBar.barTintColor = UIColor.black
            //viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = UIColor.white
        case .favorites:
            print("favorites")
            bottomBar.barTintColor = UIColor.black
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = UIColor.white
        case .search:
            print("search")
            bottomBar.barTintColor = UIColor.white
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = UIColor.black
        }
    }
}
