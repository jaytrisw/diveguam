//
//  OpenItemController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 12/21/17.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Toast_Swift
import MapKit

class OpenItemController: UIViewController, MKMapViewDelegate, UIScrollViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var grabberTop: NSLayoutConstraint!
    @IBOutlet weak var grabber: UIView!
    @IBOutlet weak var galleryStackBottom: UILabel!
    @IBOutlet weak var galleryStackTop: UILabel!
    @IBOutlet weak var galleryStacks: UIStackView!
    @IBOutlet weak var galleryStackTen: UIStackView!
    @IBOutlet weak var galleryStackNine: UIStackView!
    @IBOutlet weak var galleryStackEight: UIStackView!
    @IBOutlet weak var galleryStackSeven: UIStackView!
    @IBOutlet weak var galleryStackSix: UIStackView!
    @IBOutlet weak var galleryStackFive: UIStackView!
    @IBOutlet weak var galleryStackFour: UIStackView!
    @IBOutlet weak var galleryStackThree: UIStackView!
    @IBOutlet weak var galleryStackTwo: UIStackView!
    @IBOutlet weak var galleryStackOne: UIStackView!
    @IBOutlet weak var siteInfo: UIStackView!
    @IBOutlet weak var slideInButton: UIButton!
    @IBOutlet weak var bottomSlideIn: NSLayoutConstraint!
    @IBOutlet weak var slideInContainer: UIVisualEffectView!
    @IBOutlet weak var scrollViewTop: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var authorButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var weatherError: UILabel!
    @IBOutlet weak var tideError: UILabel!
    @IBOutlet weak var tidesIndicator: UIActivityIndicatorView!
    @IBOutlet weak var weatherIndicator: UIActivityIndicatorView!
    @IBOutlet weak var commentsLabel: UIButton!
    @IBOutlet weak var contentView: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lifeLabel: UILabel!
    @IBOutlet weak var lifeValue: UITextView!
    @IBOutlet weak var hazardLabel: UILabel!
    @IBOutlet weak var hazardsValue: UITextView!
    @IBOutlet weak var titleValue: UILabel!
    @IBOutlet weak var recommendedCert: UILabel!
    @IBOutlet weak var depthRange: UILabel!
    @IBOutlet weak var weatherType: UIImageView!
    @IBOutlet weak var weatherValue: UILabel!
    @IBOutlet weak var fahrenheit: UIImageView!
    @IBOutlet weak var tempSep: UILabel!
    @IBOutlet weak var beaufort: UIImageView!
    @IBOutlet weak var windSpeedValue: UILabel!
    @IBOutlet weak var windDirectionImage: UIImageView!
    @IBOutlet weak var windDirection: UILabel!
    @IBOutlet weak var depthTop: UILabel!
    @IBOutlet weak var depthValue: UILabel!
    @IBOutlet weak var accessValue: UILabel!
    @IBOutlet weak var certificationValue: UILabel!
    @IBOutlet weak var visibilityRange: UILabel!
    @IBOutlet weak var visibilityValue: UILabel!
    @IBOutlet weak var siteMapImage: UIImageView!
    @IBOutlet weak var firstTideType: UIImageView!
    @IBOutlet weak var firstTide: UILabel!
    @IBOutlet weak var tideSep: UILabel!
    @IBOutlet weak var secondTideType: UIImageView!
    @IBOutlet weak var secondTide: UILabel!
    
    // MARK: - Arrays
    var SingleItem:ItemNews?
    var extrasList = Array<ItemExtras>()
    var imgList = Array<ItemImages>()
    var boatList = Array<ItemBoats>()
    
    // MARK: - Variables
    var defaultImage:UIImage!
    var myView:UIView!
    var waiting:Bool = true
    var titleLabel:UILabel!
    var imageViewFull:UIImageView!
    var photographerLabel:UILabel!
    var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        scrollView.delegate = self
        setupVC()
        checkReachability()
        putData()
        putExtras()
        putImages()
        map()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.scrollViewBottom.constant = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        updateCenter()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CommentsController {
            dest.post_url = (self.SingleItem?.newUrl)!
            dest.item_id = (self.SingleItem?.id)!
            dest.web_id = (self.SingleItem?.postID)!
            dest.commentCount = (self.SingleItem?.commentCount)!
        }
        if segue.destination is GalleryViewController {
            let vc = segue.destination as? GalleryViewController
            vc?.post_url = (self.SingleItem?.newUrl)!
        }
        if let dest = segue.destination as? ProfileViewController {
            dest.post_url = (self.SingleItem?.newUrl)!
        }
    }
    
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        self.grabberTop.constant = offset * -1
        if offset < -1 {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.scrollView.layer.cornerRadius = 0
            }
        }
        if offset < -100 {
            self.scrollViewBottom.constant = -screenHeight
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.view.layoutIfNeeded()
            }
            resetRegion()
            mapInteractions()
            self.bottomSlideIn.constant = 0
            UIView.animate(withDuration: 0.1, delay: 0.5, options: .curveEaseOut, animations: { [weak self] in
                self?.view.layoutIfNeeded()
            })
        }
        if offset >= 1 {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.grabber.alpha = 0
                self?.scrollView.layer.cornerRadius = 0
            }
        }
        if offset >= 50 {

        }
        if offset == 0 {
            grabber.alpha = 1
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.scrollView.layer.cornerRadius = 20
            }
        }
    }
    
    // MARK: - Setup View Controller
    func setupVC() {
        self.scrollViewBottom.constant = -screenHeight
        UIView.animate(withDuration: 0.0) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        self.bottomSlideIn.constant = -100
        UIView.animate(withDuration: 0.0) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        defaultImage = UIImage(named:"placeholder")!
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        blurEffectView.frame.size.height = screenHeight * 4
        myView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        myView.backgroundColor = UIColor.clear
        myView.alpha = 0
        titleLabel = UILabel()
        titleLabel.textColor = UIColor .white
        titleLabel.font = UIFont (name: "AvenirNext-Regluar", size: 17)
        photographerLabel = UILabel()
        photographerLabel.textColor = UIColor .white
        photographerLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
        scrollView.insertSubview(blurEffectView, at: 0)
        scrollView.clipsToBounds = true
        scrollView.layer.cornerRadius = 20
        scrollView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        weatherValue.alpha = 0
        weatherType.alpha = 0
        fahrenheit.alpha = 0
        tempSep.alpha = 0
        beaufort.alpha = 0
        windSpeedValue.alpha = 0
        windDirectionImage.alpha = 0
        windDirection.alpha = 0
        firstTideType.alpha = 0
        firstTide.alpha = 0
        tideSep.alpha = 0
        secondTideType.alpha = 0
        secondTide.alpha = 0
        tideError.alpha = 0
        weatherError.alpha = 0
        galleryStackOne.isHidden = true
        galleryStackTwo.isHidden = true
        galleryStackThree.isHidden = true
        galleryStackFour.isHidden = true
        galleryStackFive.isHidden = true
        galleryStackSix.isHidden = true
        galleryStackSeven.isHidden = true
        galleryStackEight.isHidden = true
        galleryStackNine.isHidden = true
        galleryStackTen.isHidden = true
        //avatar.isHidden = true
        //authorButton.isHidden = true
        //ratingButton.isHidden = true
        if SingleItem?.gallery == "0" {
            galleryStacks.removeFromSuperview()
        }
    }
    
    // MARK: - Map View
    func map() {
        let lat = Double(SingleItem?.latitude ?? "") ?? 0.0
        let long = Double(SingleItem?.longitude ?? "") ?? 0.0
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = SingleItem?.title
        mapView.addAnnotation(annotation)
        mapView.isZoomEnabled = false
        mapView.isScrollEnabled = false
    }
    func updateCenter() {
        let lat = Double(SingleItem?.latitude ?? "") ?? 0.0
        let long = Double(SingleItem?.longitude ?? "") ?? 0.0
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        var center = location;
        center.latitude -= self.mapView.region.span.latitudeDelta / 3.3;
        mapView.setCenter(center, animated: true);
    }
    func resetRegion() {
        let lat = Double(SingleItem?.latitude ?? "") ?? 0.0
        let long = Double(SingleItem?.longitude ?? "") ?? 0.0
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)

    }
    func mapInteractions() {
        mapView.isZoomEnabled = !mapView.isZoomEnabled
        mapView.isScrollEnabled = !mapView.isScrollEnabled
    }

    // MARK: - API Calls
    func putData(){
        slideInButton.setTitle(SingleItem?.title, for: .normal)
        titleValue.text = SingleItem?.title
        certificationValue.text = SingleItem?.certificationLevel
        depthTop.text = SingleItem?.depthRange
        depthValue.text = SingleItem?.depthRange
        visibilityValue.text = SingleItem?.visRange
        accessValue.text = SingleItem?.access
        if (SingleItem?.siteMap != "") {
            let esc_str = SingleItem?.siteMap?
                .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            siteMapImage.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        } else {
            siteMapImage.removeFromSuperview()
            let pinTop = NSLayoutConstraint(item: commentsLabel, attribute: .top, relatedBy: .equal,
                                            toItem: contentView, attribute: .bottom, multiplier: 1.0, constant: 0)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
        if (SingleItem?.warning != "") {
        }
        if SingleItem?.marineLife != "" {
            lifeValue.text = SingleItem?.marineLife
        } else {
            lifeLabel.removeFromSuperview()
            lifeValue.removeFromSuperview()
        }
        if SingleItem?.hazards != "" {
            hazardsValue.text = SingleItem?.hazards
        } else {
            if SingleItem?.marineLife != "" {
                hazardLabel.removeFromSuperview()
                hazardsValue.removeFromSuperview()
            } else {
                hazardLabel.removeFromSuperview()
                hazardsValue.removeFromSuperview()
            }
        }
        commentsLabel.setTitle("View Comments",for: .normal)
        if SingleItem?.author != "error" {
            authorButton.setTitle(SingleItem?.author, for: .normal)
        }
        if SingleItem?.authorProRating != "error" {
            ratingButton.setTitle((SingleItem?.authorProRating!)! + " #" + (SingleItem?.authorProNumber!)!, for: .normal)
        }
        if (SingleItem?.avatar != "error") {
            let esc_str = SingleItem?.avatar?
                .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            avatar.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        let link = SingleItem?.newUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        let url = URL(string: link!)
        let request = URLRequest(url: url!)
        contentView.text = SingleItem?.newContent
        hazardsValue.textContainerInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        lifeValue.textContainerInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
    }
    func putExtras() {
        let post_url = SingleItem?.newUrl
        ApiNews.getBoatSchedule(post_url: post_url!, completion: { (data:Array<ItemBoats>, success:Bool) in
            if success {
                for item in data {
                    self.boatList.append(item)
                }
                if data.count > 0 {
                    self.waiting = false
                }
                print(self.waiting)
                if self.boatList[0].timestamp != nil {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.siteInfo.addArrangedSubview(blankLabel)
                    let textLabel = UILabel()
                    textLabel.textColor = UIColor .white
                    textLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    textLabel.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
                    textLabel.text  = "Boat Schedule"
                    textLabel.alpha = 0
                    self.siteInfo.addArrangedSubview(textLabel)
                    textLabel.fadeIn()
                }
                var i = 0
                repeat {
                    let stackView   = UIStackView()
                    stackView.axis  = NSLayoutConstraint.Axis.horizontal
                    stackView.distribution  = UIStackView.Distribution.equalSpacing
                    stackView.alignment = UIStackView.Alignment.center
                    stackView.spacing   = 8.0
                    let textLabel = UILabel()
                    textLabel.textColor = UIColor .white
                    textLabel.font = UIFont (name: "AvenirNext-Regular", size: 17)
                    textLabel.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
                    textLabel.text  = "\(self.boatList[i].timestamp!)"
                    let imageView = UIImageView()
                    imageView.heightAnchor.constraint(equalToConstant:18.0).isActive = true
                    imageView.widthAnchor.constraint(equalToConstant: 34.0).isActive = true
                    imageView.image = UIImage(named: self.boatList[i].company!)
                    stackView.addArrangedSubview(imageView)
                    stackView.addArrangedSubview(textLabel)
                    self.siteInfo.addArrangedSubview(stackView)
                    stackView.fadeIn()
                    print("Label Added")
                    i = i + 1
                } while i < self.boatList.count
            }
        })
        ApiNews.getExtras(post_url: post_url!, completion: { (data:Array<ItemExtras>, success:Bool) in
            if success {
                for item in data {
                    self.extrasList.append(item)
                }
                self.waiting = false
                if self.extrasList[0].commentCount == "0" {
                    self.commentsLabel.setTitle("Add Comment",for: .normal)
                } else if self.extrasList[0].commentCount == "error" {
                } else {
                    self.commentsLabel.setTitle("View Comments",for: .normal)
                }
                if self.extrasList[0].weather != "error" {
                    if self.extrasList[0].weather == "clear-night" {
                        self.weatherType.image = UIImage(named:"weather-clear-night")
                    } else if self.extrasList[0].weather == "light-rain" {
                        self.weatherType.image = UIImage(named:"weather-light-rain")
                    } else if self.extrasList[0].weather == "rain" {
                        self.weatherType.image = UIImage(named:"weather-rain")
                    } else if self.extrasList[0].weather == "cloudy" {
                        self.weatherType.image = UIImage(named:"weather-cloudy")
                    } else if self.extrasList[0].weather == "typhoon" {
                        self.weatherType.image = UIImage(named:"weather-typhoon")
                    } else if self.extrasList[0].weather == "thunder" {
                        self.weatherType.image = UIImage(named:"weather-thunder")
                    } else {
                        self.weatherType.image = UIImage(named:"weather-sunny")
                    }
                    self.weatherType.fadeIn()
                    self.weatherValue.text = self.extrasList[0].temperature
                    self.weatherValue.fadeIn()
                    self.fahrenheit.fadeIn()
                    self.tempSep.fadeIn()
                    if self.extrasList[0].beaufort == "beaufort-1" {
                        self.beaufort.image = UIImage(named:"beaufort-1")
                    } else if self.extrasList[0].beaufort == "beaufort-2" {
                        self.beaufort.image = UIImage(named:"beaufort-2")
                    } else if self.extrasList[0].beaufort == "beaufort-3" {
                        self.beaufort.image = UIImage(named:"beaufort-3")
                    } else if self.extrasList[0].beaufort == "beaufort-4" {
                        self.beaufort.image = UIImage(named:"beaufort-4")
                    } else if self.extrasList[0].beaufort == "beaufort-5" {
                        self.beaufort.image = UIImage(named:"beaufort-5")
                    } else if self.extrasList[0].beaufort == "beaufort-6" {
                        self.beaufort.image = UIImage(named:"beaufort-6")
                    } else if self.extrasList[0].beaufort == "beaufort-7" {
                        self.beaufort.image = UIImage(named:"beaufort-7")
                    } else if self.extrasList[0].beaufort == "beaufort-8" {
                        self.beaufort.image = UIImage(named:"beaufort-8")
                    } else if self.extrasList[0].beaufort == "beaufort-9" {
                        self.beaufort.image = UIImage(named:"beaufort-9")
                    } else if self.extrasList[0].beaufort == "beaufort-10" {
                        self.beaufort.image = UIImage(named:"beaufort-10")
                    } else if self.extrasList[0].beaufort == "beaufort-11" {
                        self.beaufort.image = UIImage(named:"beaufort-11")
                    } else if self.extrasList[0].beaufort == "beaufort-12" {
                        self.beaufort.image = UIImage(named:"beaufort-12")
                    } else {
                        self.beaufort.image = UIImage(named:"beaufort-0")
                    }
                    self.beaufort.fadeIn()
                    self.windSpeedValue.text = self.extrasList[0].windSpeed! + " mph"
                    self.windSpeedValue.fadeIn()
                    if self.extrasList[0].windDirection == "NE" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-east")
                    } else if self.extrasList[0].windDirection == "E" {
                        self.windDirectionImage.image = UIImage(named:"direction-east")
                    } else if self.extrasList[0].windDirection == "SE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-east")
                    } else if self.extrasList[0].windDirection == "S" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "SW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-west")
                    } else if self.extrasList[0].windDirection == "W" {
                        self.windDirectionImage.image = UIImage(named:"direction-west")
                    } else if self.extrasList[0].windDirection == "NW" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-west")
                    } else if self.extrasList[0].windDirection == "ENE" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-east")
                    } else if self.extrasList[0].windDirection == "ESE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-east")
                    } else if self.extrasList[0].windDirection == "SSE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "SSW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "WSW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-west")
                    } else if self.extrasList[0].windDirection == "WNW" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-west")
                    } else {
                        self.windDirectionImage.image = UIImage(named:"direction-north")
                    }
                    self.windDirectionImage.fadeIn()
                    self.windDirection.text = self.extrasList[0].windDirection
                    self.windDirection.fadeIn()
                } else {
                    self.weatherError.text = "Parsing error"
                    self.weatherError.fadeIn()
                }
                if self.extrasList[0].firstTideType != "error" {
                    if self.extrasList[0].firstTideType == "High" {
                        self.firstTideType.image = UIImage(named:"tide-high")
                    } else {
                        self.firstTideType.image = UIImage(named:"tide-low")
                    }
                    self.firstTideType.fadeIn()
                    self.firstTide.text = self.extrasList[0].firstTideHeight! + " " + self.extrasList[0].firstTideTime!
                    self.firstTide.fadeIn()
                    self.tideSep.fadeIn()
                    if self.extrasList[0].secondTideType == "High" {
                        self.secondTideType.image = UIImage(named:"tide-high")
                    } else {
                        self.secondTideType.image = UIImage(named:"tide-low")
                    }
                    self.secondTideType.fadeIn()
                    self.secondTide.text = self.extrasList[0].secondTideHeight! + " " + self.extrasList[0].secondTideTime!
                    self.secondTide.fadeIn()
                } else {
                    self.tideError.text = "Parsing error"
                    self.tideError.fadeIn()
                }
                self.weatherIndicator.stopAnimating()
                self.tidesIndicator.stopAnimating()
            } else {
                if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                    print("Failed to download JSON data")
                    self.weatherIndicator.stopAnimating()
                    self.weatherError.text = "Error parsing data"
                    self.weatherError.fadeIn()
                } else {
                    self.checkReachability()
                }
            }
        })
        
    }
    func putImages() {
        let post_url = SingleItem?.newUrl
        ApiNews.getImages(post_url: post_url!, completion: { (data:Array<ItemImages>, success:Bool) in
            var stackOneCount: Int = 0
            var stackTwoCount: Int = 0
            var stackThreeCount: Int = 0
            var stackFourCount: Int = 0
            var stackFiveCount: Int = 0
            var stackSixCount: Int = 0
            var stackSevenCount: Int = 0
            var stackEightCount: Int = 0
            var stackNineCount: Int = 0
            var stackTenCount: Int = 0
            if success {
                for item in data {
                    self.imgList.append(item)
                }
                var i = 1
                repeat {
                    let imageView = UIImageView()
                    let I = i-1
                    let esc_str = self.imgList[I].imgUrl?
                        .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                    imageView.af_setImage(
                        withURL: URL(string:esc_str!)!,
                        placeholderImage: self.defaultImage,
                        imageTransition: .crossDissolve(0.2)
                    )
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.0/1.0).isActive = true
                    imageView.contentMode = .scaleAspectFill
                    imageView.layer.cornerRadius = 4
                    imageView.clipsToBounds = true
                    let blurEffect = UIBlurEffect(style: .dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = self.myView.frame
                    self.myView.insertSubview(blurEffectView, at: 0)
                    self.view.addSubview(self.myView)
                    self.myView.alpha = 0
                    imageView.addTapGestureRecognizer {
                        let photoInfo = UIView(frame: CGRect(x: 0, y: self.screenHeight - 60, width: self.screenWidth, height: 48))
                        photoInfo.backgroundColor = UIColor .clear
                        self.myView.addSubview(photoInfo)
                        let stackView = UIStackView()
                        stackView.axis = NSLayoutConstraint.Axis.vertical
                        stackView.distribution = UIStackView.Distribution.equalSpacing
                        stackView.alignment = UIStackView.Alignment.fill
                        stackView.spacing = 8.0
                        photoInfo.addSubview(stackView)
                        self.photographerLabel.text  = "  Photo by: \(self.imgList[I].photographer!)"
                        stackView.addArrangedSubview(self.photographerLabel)
                        self.titleLabel.text  = "  Title: \(self.imgList[I].title!)"
                        stackView.addArrangedSubview(self.titleLabel)
                        stackView.translatesAutoresizingMaskIntoConstraints = false
                        self.imageViewFull = UIImageView()
                        self.imageViewFull.af_setImage(
                            withURL: URL(string:esc_str!)!)
                        self.imageViewFull.translatesAutoresizingMaskIntoConstraints = false
                        self.imageViewFull.heightAnchor.constraint(equalToConstant: self.screenHeight).isActive = true
                        self.imageViewFull.widthAnchor.constraint(equalToConstant: self.screenWidth).isActive = true
                        self.imageViewFull.contentMode = .scaleAspectFit
                        self.myView.addSubview(self.imageViewFull)
                        
                        let button = UIButton()
                        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
                            button.frame = CGRect(x: 4, y: 44, width: 44, height: 44)
                        } else {
                            button.frame = CGRect(x: 3, y: 20, width: 44, height: 44)
                        }
                        button.setImage(UIImage(named: "close"), for: .normal)
                        button.tintColor = UIColor.white
                        button.addTarget(self, action: #selector(self.closeImageAction), for: .touchUpInside)
                        self.myView.addSubview(button)
                        self.myView.fadeIn()
                    }
                    if i == 1 || i == 2 || i == 3 {
                        print("First row")
                        stackOneCount = stackOneCount + 1
                        print(stackOneCount)
                        self.galleryStackOne.isHidden = false
                        self.galleryStackOne.addArrangedSubview(imageView)
                    }
                    if i == 4 || i == 5 || i == 6 {
                        print("Second row")
                        stackTwoCount = stackTwoCount + 1
                        print(stackTwoCount)
                        self.galleryStackTwo.isHidden = false
                        self.galleryStackTwo.addArrangedSubview(imageView)
                    }
                    if i == 7 || i == 8 || i == 9 {
                        print("Third row")
                        stackThreeCount = stackThreeCount + 1
                        print(stackThreeCount)
                        self.galleryStackThree.isHidden = false
                        self.galleryStackThree.addArrangedSubview(imageView)
                    }
                    if i == 10 || i == 11 || i == 12 {
                        print("Forth row")
                        stackFourCount = stackFourCount + 1
                        print(stackFourCount)
                        self.galleryStackFour.isHidden = false
                        self.galleryStackFour.addArrangedSubview(imageView)
                    }
                    if i == 13 || i == 14 || i == 15 {
                        print("Fifth row")
                        stackFiveCount = stackFiveCount + 1
                        print(stackFiveCount)
                        self.galleryStackFive.isHidden = false
                        self.galleryStackFive.addArrangedSubview(imageView)
                    }
                    if i == 16 || i == 17 || i == 18 {
                        print("Sixth row")
                        stackSixCount = stackSixCount + 1
                        print(stackSixCount)
                        self.galleryStackSix.isHidden = false
                        self.galleryStackSix.addArrangedSubview(imageView)
                    }
                    if i == 19 || i == 20 || i == 21 {
                        print("Seventh row")
                        stackSevenCount = stackSevenCount + 1
                        print(stackSevenCount)
                        self.galleryStackSeven.isHidden = false
                        self.galleryStackSeven.addArrangedSubview(imageView)
                    }
                    if i == 22 || i == 23 || i == 24 {
                        print("Eighth row")
                        stackEightCount = stackEightCount + 1
                        print(stackEightCount)
                        self.galleryStackEight.isHidden = false
                        self.galleryStackEight.addArrangedSubview(imageView)
                    }
                    if i == 25 || i == 26 || i == 27 {
                        print("Nineth row")
                        stackNineCount = stackNineCount + 1
                        print(stackNineCount)
                        self.galleryStackNine.isHidden = false
                        self.galleryStackNine.addArrangedSubview(imageView)
                    }
                    if i == 28 || i == 29 || i == 30 {
                        print("Tenth row")
                        stackTenCount = stackTenCount + 1
                        print(stackTenCount)
                        self.galleryStackTen.isHidden = false
                        self.galleryStackTen.addArrangedSubview(imageView)
                    }
                    print("Image Added")
                    i = i + 1
                } while i <= self.imgList.count
                if stackOneCount == 0 {
                    self.galleryStackOne.removeFromSuperview()
                    print("Gallery stack(1) removed from superview")
                }
                if stackTwoCount == 0 {
                    self.galleryStackTwo.removeFromSuperview()
                    print("Gallery stack(2) removed from superview")
                }
                if stackThreeCount == 0 {
                    self.galleryStackThree.removeFromSuperview()
                    print("Gallery stack(3) removed from superview")
                }
                if stackFourCount == 0 {
                    self.galleryStackFour.removeFromSuperview()
                    print("Gallery stack(4) removed from superview")
                }
                if stackFiveCount == 0 {
                    self.galleryStackFive.removeFromSuperview()
                    print("Gallery stack(5) removed from superview")
                }
                if stackSixCount == 0 {
                    self.galleryStackSix.removeFromSuperview()
                    print("Gallery stack(6) removed from superview")
                }
                if stackSevenCount == 0 {
                    self.galleryStackSeven.removeFromSuperview()
                    print("Gallery stack(7) removed from superview")
                }
                if stackEightCount == 0 {
                    self.galleryStackEight.removeFromSuperview()
                    print("Gallery stack(8) removed from superview")
                }
                if stackNineCount == 0 {
                    self.galleryStackNine.removeFromSuperview()
                    print("Gallery stack(9) removed from superview")
                }
                if stackTenCount == 0 {
                    self.galleryStackTen.removeFromSuperview()
                    print("Gallery stack(10) removed from superview")
                }
                if stackOneCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackOne.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackTwoCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackTwo.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackThreeCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackThree.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackFourCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackFour.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackFiveCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackFive.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackSixCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackSix.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackSevenCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackSeven.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackEightCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackEight.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackNineCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackNine.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackTenCount == 1 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackTen.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackOneCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackOne.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackTwoCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackTwo.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackThreeCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackThree.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackFourCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackFour.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackFiveCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackFive.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackSixCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackSix.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackSevenCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackSeven.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackEightCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackEight.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackNineCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackNine.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
                if stackTenCount <= 2 {
                    let blankLabel = UILabel()
                    blankLabel.textColor = UIColor .clear
                    blankLabel.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    blankLabel.heightAnchor.constraint(equalToConstant: 10.0).isActive = true
                    blankLabel.text  = " "
                    self.galleryStackTen.addArrangedSubview(blankLabel)
                    print("Added label to fix UI yuckiness")
                }
            } else {
                print("This is the error block")
            }
        })
    }

    // MARK: - Misc Functions
    func shareTxt() {
        let text = "\(SingleItem!.title ?? "Error") \n \(SingleItem!.newUrl ?? "Error")"
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    func saveToDB() {
        let dbHandler = DBHandler()
        dbHandler.beforeStart()
        
        if dbHandler.insertNews(item: SingleItem!){
            self.view.makeToast("Dive site saved.")
        }else{
            self.view.makeToast("This site has already been saved.")
        }
    }
    func deleteFromDB() {
        let dbHandler = DBHandler()
        dbHandler.beforeStart()
        
        if dbHandler.deleteNews(item: SingleItem!){
            self.view.makeToast("Item Deleted successfully")
            NotificationCenter.default.post(name: NSNotification.Name("load"), object: nil)
            dismiss(animated: true, completion: nil)
        }else{
            self.view.makeToast("Sorry there is some error ")
        }
    }
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            weatherIndicator.stopAnimating()
            tidesIndicator.stopAnimating()
            tideError.text = "Connection error"
            weatherError.text = "Connection error"
            tideError.fadeIn()
            weatherError.fadeIn()
        }
    }
    
    // MARK: - Actions
    @IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                let frameHeight = self.slideInContainer.frame.size.height
                print(initialTouchPoint.y)
                self.slideInContainer.frame.size.height = frameHeight + initialTouchPoint.y
                UIView.animate(withDuration: 0.5) { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y < -100 {
                var screenHeight: CGFloat {
                    return UIScreen.main.bounds.height
                }
                self.scrollViewBottom.constant = 0
                UIView.animate(withDuration: 0.5) { [weak self] in
                    self?.view.layoutIfNeeded()
                }
                updateCenter()
                mapInteractions()
                self.bottomSlideIn.constant = -100
                UIView.animate(withDuration: 0.1) { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        self.scrollViewBottom.constant = -screenHeight
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
    @IBAction func shareButton(_ sender: Any) {
        self.shareTxt()
    }
    @IBAction func saveButton(_ sender: Any) {
        if (SingleItem?.newSaved)! {
            self.deleteFromDB()
        } else {
            self.saveToDB()
        }
    }
    @IBAction func commentButton(_ sender: Any) {
        self.performSegue(withIdentifier: "open_comments", sender: nil)
    }
    @IBAction func slideIn(_ sender: Any) {
        var screenHeight: CGFloat {
            return UIScreen.main.bounds.height
        }
        self.scrollViewBottom.constant = 0
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        updateCenter()
        mapInteractions()
        self.bottomSlideIn.constant = -100
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
    @IBAction func mapControl(_ sender: Any) {
        switch ((sender as AnyObject).selectedSegmentIndex) {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .satellite
        default:
            mapView.mapType = .standard
        }
    }
    @objc func closeImageAction(sender: UIButton!) {
        myView.fadeOut()
        self.titleLabel.text = nil
        self.photographerLabel.text = nil
        self.imageViewFull = nil

    }
}
extension UIView {
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    fileprivate typealias Action = (() -> Void)?
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}
