//
//  ApiNews.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 1/16/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiNews: NSObject {
    class func getNewsByCategory(page:Int,count:Int,id:String,
                                 completion:@escaping (_ data:Array<ItemNews>,_ success:Bool)->Void){
        var NewsList = Array<ItemNews>()
        let url = "\(MyData.wpLink)get_category_posts\(MyData.apiKey)"
        print (url)
        let params = ["page": "\(page)", "count": "\(count)", "id": "\(id)"]
        print(params)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    print(url)
                    completion(NewsList,false)
                case .success(let value):
                    let json = JSON(value)
                    if let posts = json["posts"].array{
                        for post in posts {
                            let id = post["id"].int ?? 0
                            let title = post["title"].string ?? "error"
                            let datetime = post["date"].string ?? "error"
                            let newUrl = post["url"].string ?? "error"
                            let newContent = post["content_plain"].string ?? "error"
                            let commentCount = post["comment_count"].int ?? 0
                            let gallery = post["gallery"].string ?? "0"
                            var img_url = post["image"].string
                            let certLevel = post["site_info"]["certification_level"].string ?? "error"
                            let depth = post["site_info"]["depth"].string ?? "error"
                            let visibility = post["site_info"]["visibility"].string ?? "error"
                            let access = post["site_info"]["access"].string ?? "error"
                            let marineLife = post["site_info"]["marine_life"].string ?? "error"
                            let hazards = post["site_info"]["hazards"].string ?? "error"
                            let siteMap = post["site_info"]["divesite_map"].string ?? ""
                            let lat = post["latitude"].string ?? "error"
                            let long = post["longitude"].string ?? "error"
                            let city = post["sub_title"].string ?? "error"
                            let firstAid = post["first_aid"]["enabled"].string ?? "0"
                            let firstAidTitle = post["first_aid"]["title"].string ?? "error"
                            let firstAidContent = post["first_aid"]["content"].string ?? "error"
                            let firstAidImageUrl = post["first_aid"]["image_url"].string ?? "error"
                            let firstAidSymptoms = post["first_aid"]["symptoms"].string ?? "error"
                            let firstAidDescription = post["first_aid"]["description"].string ?? "error"
                            let warning = post["warning"]["enabled"].string ?? "0"
                            let warningContact = post["warning"]["contact"].string ?? "error"
                            let warningLabel = post["warning"]["label"].string ?? "error"
                            let warningSubject = post["warning"]["subject"].string ?? "error"
                            let warningInstructions = post["warning"]["instructions"].string ?? "error"
                            let avatar = post["author"]["avatar"].string ?? "error"
                            let author = post["author"]["name"].string ?? "error"
                            let authorProRating = post["author"]["professional_title"].string ?? "error"
                            let authorProNumber = post["author"]["professional_number"].string ?? "error"
                            if img_url == nil {
                                img_url = post["thumbnail_images"]["full"]["url"].string ?? "error"
                            }
                            
                            NewsList.append(ItemNews(
                                id: "\(id)",
                                postID: "\(id)",
                                title: title.html2String,
                                imgUrl: img_url!,
                                datetime: datetime,
                                writer: author,
                                newUrl: newUrl,
                                newContent: newContent,
                                depthRange: depth,
                                visRange: visibility,
                                latitude: lat,
                                longitude: long,
                                city: city,
                                certificationLevel: certLevel,
                                access: access,
                                hazards: hazards,
                                siteMap: siteMap,
                                commentCount: "\(commentCount)",
                                marineLife: marineLife,
                                gallery: gallery,
                                warning: warning,
                                warningContact: warningContact,
                                warningLabel: warningLabel,
                                warningSubject: warningSubject,
                                warningInstructions: warningInstructions,
                                firstAid: firstAid,
                                firstAidTitle: firstAidTitle,
                                firstAidContent: firstAidContent,
                                firstAidImageUrl: firstAidImageUrl,
                                firstAidSymptoms: firstAidSymptoms,
                                firstAidDescription: firstAidDescription,
                                avatar: avatar,
                                author: author,
                                authorProRating: authorProRating,
                                authorProNumber: authorProNumber
                            ))
                        }
                        completion(NewsList,true)
                    }
                }
        }
    }
    
    class func getNewsByAll(page:Int,count:Int,
                                 completion:@escaping (_ data:Array<ItemNews>,_ success:Bool)->Void){
        var NewsList = Array<ItemNews>()
        let url = "https://www.guamdiveguide.com/tag/popular?json=1\(MyData.apiKey)"
        let params = ["page": "\(page)", "count": "10"]
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(NewsList,false)
                case .success(let value):
                    let json = JSON(value)
                    if let posts = json["posts"].array{
                        for post in posts {
                            let id = post["id"].int ?? 0
                            let title = post["title"].string ?? "error"
                            let datetime = post["date"].string ?? "error"
                            let newUrl = post["url"].string ?? "error"
                            let newContent = post["content_plain"].string ?? "error"
                            let commentCount = post["comment_count"].int ?? 0
                            let gallery = post["gallery"].string ?? "0"
                            var img_url = post["image"].string
                            let certLevel = post["site_info"]["certification_level"].string ?? "error"
                            let depth = post["site_info"]["depth"].string ?? "error"
                            let visibility = post["site_info"]["visibility"].string ?? "error"
                            let access = post["site_info"]["access"].string ?? "error"
                            let marineLife = post["site_info"]["marine_life"].string ?? "error"
                            let hazards = post["site_info"]["hazards"].string ?? "error"
                            let siteMap = post["site_info"]["divesite_map"].string ?? ""
                            let lat = post["latitude"].string ?? "error"
                            let long = post["longitude"].string ?? "error"
                            let city = post["sub_title"].string ?? "error"
                            let firstAid = post["first_aid"]["enabled"].string ?? "0"
                            let firstAidTitle = post["first_aid"]["title"].string ?? "error"
                            let firstAidContent = post["first_aid"]["content"].string ?? "error"
                            let firstAidImageUrl = post["first_aid"]["image_url"].string ?? "error"
                            let firstAidSymptoms = post["first_aid"]["symptoms"].string ?? "error"
                            let firstAidDescription = post["first_aid"]["description"].string ?? "error"
                            let warning = post["warning"]["enabled"].string ?? "0"
                            let warningContact = post["warning"]["contact"].string ?? "error"
                            let warningLabel = post["warning"]["label"].string ?? "error"
                            let warningSubject = post["warning"]["subject"].string ?? "error"
                            let warningInstructions = post["warning"]["instructions"].string ?? "error"
                            let avatar = post["author"]["avatar"].string ?? "error"
                            let author = post["author"]["name"].string ?? "error"
                            let authorProRating = post["author"]["professional_title"].string ?? "error"
                            let authorProNumber = post["author"]["professional_number"].string ?? "error"
                            if img_url == nil {
                                img_url = post["thumbnail_images"]["full"]["url"].string ?? "error"
                            }
                            NewsList.append(ItemNews(
                                id: "\(id)",
                                postID: "\(id)",
                                title: title.html2String,
                                imgUrl: img_url! ,
                                datetime: datetime,
                                writer: author,
                                newUrl: newUrl,
                                newContent: newContent,
                                depthRange: depth,
                                visRange: visibility,
                                latitude: lat,
                                longitude: long,
                                city: city,
                                certificationLevel: certLevel,
                                access: access,
                                hazards: hazards,
                                siteMap: siteMap,
                                commentCount: "\(commentCount)",
                                marineLife: marineLife,
                                gallery: gallery,
                                warning: warning,
                                warningContact: warningContact,
                                warningLabel: warningLabel,
                                warningSubject: warningSubject,
                                warningInstructions: warningInstructions,
                                firstAid: firstAid,
                                firstAidTitle: firstAidTitle,
                                firstAidContent: firstAidContent,
                                firstAidImageUrl: firstAidImageUrl,
                                firstAidSymptoms: firstAidSymptoms,
                                firstAidDescription: firstAidDescription,
                                avatar: avatar,
                                author: author,
                                authorProRating: authorProRating,
                                authorProNumber: authorProNumber
                            ))
                        }
                        completion(NewsList,true)
                    }
                }
        }
    }
    class func getSearchResults(searchTerm:String, page:Int,count:Int,
                            completion:@escaping (_ data:Array<ItemNews>,_ success:Bool)->Void){
        var NewsList = Array<ItemNews>()
        let url = "https://www.guamdiveguide.com/?s=\(searchTerm)&json=1\(MyData.apiKey)"
        print(url)
        let params = ["page": "\(page)", "count": "100"]
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(NewsList,false)
                case .success(let value):
                    let json = JSON(value)
                    if let posts = json["posts"].array{
                        for post in posts {
                            let id = post["id"].int ?? 0
                            let title = post["title"].string ?? "error"
                            let datetime = post["date"].string ?? "error"
                            let newUrl = post["url"].string ?? "error"
                            let newContent = post["content_plain"].string ?? "error"
                            let commentCount = post["comment_count"].int ?? 0
                            let gallery = post["gallery"].string ?? "0"
                            var img_url = post["image"].string
                            let certLevel = post["site_info"]["certification_level"].string ?? "error"
                            let depth = post["site_info"]["depth"].string ?? "error"
                            let visibility = post["site_info"]["visibility"].string ?? "error"
                            let access = post["site_info"]["access"].string ?? "error"
                            let marineLife = post["site_info"]["marine_life"].string ?? "error"
                            let hazards = post["site_info"]["hazards"].string ?? "error"
                            let siteMap = post["site_info"]["divesite_map"].string ?? ""
                            let lat = post["latitude"].string ?? "error"
                            let long = post["longitude"].string ?? "error"
                            let city = post["sub_title"].string ?? "error"
                            let firstAid = post["first_aid"]["enabled"].string ?? "0"
                            let firstAidTitle = post["first_aid"]["title"].string ?? "error"
                            let firstAidContent = post["first_aid"]["content"].string ?? "error"
                            let firstAidImageUrl = post["first_aid"]["image_url"].string ?? "error"
                            let firstAidSymptoms = post["first_aid"]["symptoms"].string ?? "error"
                            let firstAidDescription = post["first_aid"]["description"].string ?? "error"
                            let warning = post["warning"]["enabled"].string ?? "0"
                            let warningContact = post["warning"]["contact"].string ?? "error"
                            let warningLabel = post["warning"]["label"].string ?? "error"
                            let warningSubject = post["warning"]["subject"].string ?? "error"
                            let warningInstructions = post["warning"]["instructions"].string ?? "error"
                            let avatar = post["author"]["avatar"].string ?? "error"
                            let author = post["author"]["name"].string ?? "error"
                            let authorProRating = post["author"]["professional_title"].string ?? "error"
                            let authorProNumber = post["author"]["professional_number"].string ?? "error"
                            if img_url == nil {
                                img_url = post["thumbnail_images"]["full"]["url"].string ?? "error"
                            }
                            NewsList.append(ItemNews(
                                id: "\(id)",
                                postID: "\(id)",
                                title: title.html2String,
                                imgUrl: img_url! ,
                                datetime: datetime,
                                writer: author,
                                newUrl: newUrl,
                                newContent: newContent,
                                depthRange: depth,
                                visRange: visibility,
                                latitude: lat,
                                longitude: long,
                                city: city,
                                certificationLevel: certLevel,
                                access: access,
                                hazards: hazards,
                                siteMap: siteMap,
                                commentCount: "\(commentCount)",
                                marineLife: marineLife,
                                gallery: gallery,
                                warning: warning,
                                warningContact: warningContact,
                                warningLabel: warningLabel,
                                warningSubject: warningSubject,
                                warningInstructions: warningInstructions,
                                firstAid: firstAid,
                                firstAidTitle: firstAidTitle,
                                firstAidContent: firstAidContent,
                                firstAidImageUrl: firstAidImageUrl,
                                firstAidSymptoms: firstAidSymptoms,
                                firstAidDescription: firstAidDescription,
                                avatar: avatar,
                                author: author,
                                authorProRating: authorProRating,
                                authorProNumber: authorProNumber
                            ))
                        }
                        completion(NewsList,true)
                    }
                }
        }
    }
    class func getCategories(completion:@escaping (_ data:Array<ItemCat>,_ success:Bool)->Void){
        var catList = Array<ItemCat>()
        let url = "\(MyData.wpLink)core/get_category_index\(MyData.apiKey)"
        print (url)
        let params = ["param": "param"]
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(catList,false)
                case .success(let value):
                    let json = JSON(value)
                    if let posts = json["categories"].array{
                        for post in posts {
                            var img_url = post["image"].string
                            let title = post["title"].string ?? "error"
                            let id = post["id"].int ?? 0
                            let postCount = post["post_count"].int ?? 0
                            if img_url == nil {
                                img_url = post["thumbnail_images"]["full"]["url"].string ?? "error"
                            }
                           
                            catList.append(ItemCat(
                                id: "\(id)",
                                title: title,
                                postsCount:"\(postCount)",
                                imageUrl: img_url!
                            ))
                        }
                        completion(catList,true)
                    }
                }
        }
    }
    class func getExtras(post_url:String ,completion:@escaping (_ data:Array<ItemExtras>,_ success:Bool)->Void){
        var extrasList = Array<ItemExtras>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(extrasList,false)
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                    //print(post)
                            let commentCount = post["comment_count"].int ?? 0
                            let weather = post["weather_info"]["weather"].string ?? "error"
                            let temperature = post["weather_info"]["temperature"].string ?? "error"
                            let windSpeed = post["weather_info"]["wind_speed"].string ?? "error"
                            let beaufort = post["weather_info"]["beaufort"].string ?? "error"
                            let windDirection = post["weather_info"]["wind_direction"].string ?? "error"
                            let firstTideType = post["tides"]["first_type"].string ?? "error"
                            let firstTideHeight = post["tides"]["first_height"].string ?? "error"
                            let firstTideTime = post["tides"]["first_time"].string ?? "error"
                            let secondTideType = post["tides"]["second_type"].string ?? "error"
                            let secondTideHeight = post["tides"]["second_height"].string ?? "error"
                            let secondTideTime = post["tides"]["second_time"].string ?? "error"
                            let avatar = post["author"]["avatar"].string ?? "error"
                            let author = post["author"]["name"].string ?? "error"
                            let authorProRating = post["author"]["professional_title"].string ?? "error"
                            let authorProNumber = post["author"]["professional_number"].string ?? "error"
                            extrasList.append(ItemExtras(
                                commentCount: "\(commentCount)",
                                weather: weather,
                                temperature: temperature,
                                beaufort: beaufort,
                                windSpeed: windSpeed,
                                windDirection: windDirection,
                                firstTideType: firstTideType,
                                firstTideHeight: firstTideHeight,
                                firstTideTime: firstTideTime,
                                secondTideType: secondTideType,
                                secondTideHeight: secondTideHeight,
                                secondTideTime: secondTideTime,
                                avatar: avatar,
                                author: author,
                                authorProRating: authorProRating,
                                authorProNumber: authorProNumber
                            ))
                    completion(extrasList,true)
            }
        }
    }
    class func getProfile(post_url:String ,completion:@escaping (_ data:Array<ProfileFields>,_ success:Bool)->Void){
        var profileFields = Array<ProfileFields>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(profileFields,false)
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                    //print(post)
                    let avatar = post["author"]["avatar"].string ?? "error"
                    let author = post["author"]["name"].string ?? "error"
                    let authorProRating = post["author"]["professional_title"].string ?? "error"
                    let authorProNumber = post["author"]["professional_number"].string ?? "error"
                    let phoneNumber = post["author"]["phone_number"].string ?? "error"
                    let emailAddress = post["author"]["email"].string ?? "error"
                    let urlLink = post["author"]["url"].string ?? "error"
                    let urlAltLink = post["author"]["url_alt"].string ?? "error"
                    let specialtyRatings = post["author"]["specialty_ratings"].string ?? "error"
                    let headerUrl = post["author"]["header_image"].string ?? "error"
                    profileFields.append(ProfileFields(
                        avatar: avatar,
                        author: author,
                        authorProRating: authorProRating,
                        authorProNumber: authorProNumber,
                        phoneNumber: phoneNumber,
                        emailAddress: emailAddress,
                        url: urlLink,
                        urlAlt: urlAltLink,
                        specialtyRatings: specialtyRatings.html2String,
                        headerUrl: headerUrl
                    ))
                    completion(profileFields,true)
                }
        }
    }
    class func getBoatSchedule(post_url:String ,completion:@escaping (_ data:Array<ItemBoats>,_ success:Bool)->Void){
        var boatList = Array<ItemBoats>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(boatList,false)
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                    // print(post)
                    if let posts = post["boat_schedule"].array{
                        for post in posts {
                            let company = post["operator"].string ?? "error"
                            let timestamp = post["timestamp"].string ?? "error"
                            boatList.append(ItemBoats(
                                company: company,
                                timestamp: timestamp
                            ))
                        }
                        completion(boatList,true)
                    }
                }
        }
    }
    class func getImages(post_url:String ,completion:@escaping (_ data:Array<ItemImages>,_ success:Bool)->Void){
        var imgList = Array<ItemImages>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(imgList,false)
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                    if let posts = post["gallery_images"].array{
                        for post in posts {
                            let link = post["link"].string ?? "error"
                            let photographer = post["photographer"].string ?? "error"
                            let photographerLink = post["photographer_link"].string ?? "error"
                            let imgUrl = post["image"]["url"].string ?? "error"
                            let title = post["image"]["title"].string ?? "error"
                            imgList.append(ItemImages(
                                link: link,
                                photographer: photographer,
                                photographerLink: photographerLink,
                                imgUrl: imgUrl,
                                title: title
                            ))
                        }
                    completion(imgList,true)
                }
            }
        }
    }
    class func getCertifications(post_url:String ,completion:@escaping (_ data:Array<ItemCertifications>,_ success:Bool)->Void){
        var certList = Array<ItemCertifications>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(certList,false)
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                    // print(post)
                    if let posts = post["taxonomy_certification"].array{
                        for post in posts {
                            let id = post["id"].string ?? "error"
                            let title = post["title"].string ?? "error"
                            let description = post["description"].string ?? "error"
                            let image = post["image"].string ?? "error"
                            certList.append(ItemCertifications(
                                id: id,
                                title: title,
                                description: description,
                                image: image
                            ))
                        }
                        completion(certList,true)
                    }
                }
        }
    }
    class func getExtraFields(post_url:String ,completion:@escaping (_ data:Array<rawJSON>,_ success:Bool)->Void){
        var extraFieldList = Array<rawJSON>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        // print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(extraFieldList,false)
                case .success(let value):
                    let json = JSON(value)
                    //let post = json["post"]
                    if let posts = json["posts"].array{
                    //if let posts = post["gallery_images"].array{
                        extraFieldList.append(rawJSON(
                            json: posts
                        ))
                        completion(extraFieldList,true)
                    }
                }
        }
    }
    class func getComments(post_url:String ,completion:@escaping (_ data:Array<ItemComment>,_ success:Bool,_ statement:String)->Void){
        var comList = Array<ItemComment>()
        let url = "\(post_url)?json=1\(MyData.apiKey)"
        let params = ["param":"params"]
        print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(comList,false,"close")
                case .success(let value):
                    let json = JSON(value)
                    let post = json["post"]
                
                    let commentState:String = post["comment_status"].string!
                    // should be "open"
                    if let comments = post["comments"].array {
                        for comment in comments {
                            let proNumber = comment["professional_number"].string ?? "error"
                            let proTitle = comment["professional_title"].string ?? "error"
                            let com = ItemComment()
                            com.comment = comment["content"].string!
                            com.comment_id = "\(comment["id"].int)"
                            com.date = comment["date"].string!
                            com.user_fullname = comment["name"].string!
                            com.comment_avatar = comment["avatar"].string!
                            com.comment_professional_number = proNumber
                            com.comment_professional_title = proTitle
                            comList.append(com)
                        }
                    }
                    completion(comList,true,commentState)
                }
                
        }
    }
    class func addComment(comment:String,email:String,name:String,item_id:Int,completion:@escaping (_ success:Bool)-> Void){
        var comList = Array<ItemComment>()
        let url = "\(MyData.wpLink)submit_comment\(MyData.apiKey)"
        let params = ["post_id":"\(item_id)","name":"\(name)","email":"\(email)","content":"\(comment)"]
        print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(false)
                case .success(let value):
                    print(value)
                    completion(true)
                }
        }
    }
    class func sendToken(token:String,completion:@escaping (_ success:Bool)-> Void){
        var comList = Array<ItemComment>()
        let url = "\(MyData.wpNotifLink)?rest_route=/apnwp/register&os_type=android&device_token=\(token)"
        let params = ["param":"param"]
        print("This is the registration call")
        print(url)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON{ response in
                switch response.result {
                case .failure( let error ):
                    print(error)
                    completion(false)
                case .success(let value):
                    print(value)
                    completion(true)
                }
        }
    }
}
