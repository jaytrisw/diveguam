//
//  OpenCategoryViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/4/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class OpenCategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var navTitle: UINavigationItem!
    var category:ItemCat?
    
    @IBAction func exitPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var NewsList = Array<ItemNews>()
    var id:String=""
    var defaultImage:UIImage!
    var  waiting:Bool = true
    var page:Int = 1
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var noConnectionLabel: UILabel!
    private let refreshControl = UIRefreshControl()
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewList: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noConnectionLabel.alpha = 0
        refreshButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        refreshButton.alpha = 0
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        navTitle.title = category?.title
        
        putData()
        //addRefresh()
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim, height: 75)
        defaultImage = UIImage(named:"placeholder")!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NewsList[indexPath.row].newSaved = false
        if (NewsList[indexPath.row].warning == "1") {
            performSegue(withIdentifier: "OpenItemSpecial", sender: NewsList[indexPath.row])
        } else if (NewsList[indexPath.row].firstAid == "1") {
            performSegue(withIdentifier: "OpenFirstAideItem", sender: NewsList[indexPath.row])
        } else {
            performSegue(withIdentifier: "OpenSite", sender: NewsList[indexPath.row])
        } 
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenItemController {
            if let News=sender as? ItemNews {
                dest.SingleItem=News
            }
        }
        if let dest = segue.destination as? DiveSiteViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
        if let dest = segue.destination as? OpenItemWarningViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem=News
            }
        }
        if let dest = segue.destination as? OpenFirstAidItemViewController {
            if let News=sender as? ItemNews {
                dest.SingleItem = News
            }
        }
    }
    func putData() {
        CollectionViewList.reloadData()
        loadingIndicator.startAnimating()
        ApiNews.getNewsByCategory(page: page, count: 100, id: (self.category?.id)!, completion: { (data:Array<ItemNews>, success:Bool) in
            if success {
                for item in data {
                    self.NewsList.append(item)
                }
                self.loadingIndicator.stopAnimating()
                self.CollectionViewList.reloadData()
                self.waiting = false
            } else {
                if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                    print("Failed to download JSON data")
                    self.loadingIndicator.stopAnimating()
                    self.CollectionViewList.reloadData()
                    self.noConnectionLabel.text = "Error parsing data"
                    self.noConnectionLabel.alpha = 1
                    self.refreshButton.alpha = 1
                } else {
                    self.checkReachability()
                }
            }
        })
        self.refreshControl.endRefreshing()
    }
    
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            self.CollectionViewList.reloadData()
            noConnectionLabel.text = "No internet connection."
            noConnectionLabel.alpha = 1
            refreshButton.alpha = 1
        }
    }
    
    @objc func buttonAction(sender: UIButton!) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.NewsList.count - 1 && !self.waiting  {  //numberofitem count
            page = page+1
            updateNextSet(page: page)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_one", for: indexPath) as! CollectionViewCell
        // prepare
        cell.layimage.af_cancelImageRequest()
        cell.layimage.layer.removeAllAnimations()
        cell.layimage.image = nil
        // end prepare
        cell.laytitle.text=NewsList[indexPath.row].title!
        if NewsList[indexPath.row].title == "First Aid" {
            cell.cityLabel.text = "First Aid"
        // } else if NewsList[indexPath.row].title == "Warning" {
        } else {
            cell.cityLabel.text=NewsList[indexPath.row].city!
        }
        let esc_str = NewsList[indexPath.row].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        if(NewsList[indexPath.row].imgUrl == nil || NewsList[indexPath.row].imgUrl == ""){
            cell.layimage.image = defaultImage
        }else{
            cell.layimage.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        if (NewsList[indexPath.row].warning == "1") {
            cell.layimage.image = UIImage(named:"warning")!
            cell.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        } else {
            cell.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                print("Portrait")
            case .landscapeLeft,.landscapeRight :
                print("Landscape")
            default:
                print("Anything But Portrait")
            }
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            let dim = self.view.frame.size.width
            self.flowlayout.itemSize = CGSize(width: dim-1, height: 297)
        })
        super.viewWillTransition(to: size, with: coordinator)
    }

    func updateNextSet(page : Int ){
        self.waiting = true
        ApiNews.getNewsByCategory(page: page, count: 75, id: (self.category?.id)!, completion: { (data:Array<ItemNews>, success:Bool) in
            if success {
                for item in data {
                    self.NewsList.append(item)
                }
                self.CollectionViewList.reloadData()
                if data.count > 0 {
                    self.waiting = false
                }
            }
        })
    }
    
    
    func addRefresh()  {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            CollectionViewList.refreshControl = refreshControl
        } else {
            CollectionViewList.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing dive sites...", attributes: attributes)
        
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        fetchWeatherData()
    }
    
    private func fetchWeatherData() {
        checkReachability()
        self.NewsList.removeAll()
        self.CollectionViewList.reloadData()
        putData()
    }
}
