//
//  CategoryCell.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/4/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var nbrPosts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
