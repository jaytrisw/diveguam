//
//  CategoriesController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/4/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CategoriesController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var cats = Array<ItemCat>()
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var noConnectionLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    var defaultImage:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImage = UIImage(named:"placeholder")!
        noConnectionLabel.alpha = 0
        refreshButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        refreshButton.alpha = 0
        
        self.tableview.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.tableview.backgroundView = blurEffectView
        self.tableview.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)

        putData()
        checkReachability()
        // Do any additional setup after loading the view.
        tableview.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell_cat", for: indexPath) as! CategoryCell
        cell.title.text = cats[indexPath.row].title
        if cats[indexPath.row].title == "First Aid" {
            if cats[indexPath.row].postsCount == "1" {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) item"
            } else {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) items"
        }
        } else if cats[indexPath.row].title == "Warnings" {
            if cats[indexPath.row].postsCount == "1" {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) warning"
            } else {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) warnings"
            }
        } else {
            if cats[indexPath.row].postsCount == "1" {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) dive site"
            } else {
                cell.nbrPosts.text = "\(cats[indexPath.row].postsCount!) dive sites"
            }
        }
        let esc_str = cats[indexPath.row].imageUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if(cats[indexPath.row].imageUrl == nil || cats[indexPath.row].imageUrl == ""){
            cell.categoryImage.image = defaultImage
        } else {
            cell.categoryImage.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        cell.separatorInset = UIEdgeInsets.zero;
        cell.selectionStyle = .none
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "open_cat", sender: cats[indexPath.row])
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenCategoryViewController {
            if let cat = sender as? ItemCat {
                dest.category = cat
            }
        }
    }
    func putData() {
        loadingIndicator.startAnimating()
        ApiNews.getCategories(completion: { (data:Array<ItemCat>, success:Bool) in
            if success {
                for item in data {
                    self.cats.append(item)
                }
                self.loadingIndicator.stopAnimating()
                self.tableview.reloadData()
            } else {
                if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                    print("Failed to download JSON data")
                    self.loadingIndicator.stopAnimating()
                    self.tableview.reloadData()
                    self.noConnectionLabel.text = "Error parsing data"
                    self.noConnectionLabel.alpha = 1
                    self.refreshButton.alpha = 1
                } else {
                    self.checkReachability()
                }
            }
        })
    }
    
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            self.tableview.reloadData()
            noConnectionLabel.text = "No internet connection."
            noConnectionLabel.alpha = 1
            refreshButton.alpha = 1
        }
    }
    
    @objc func buttonAction(sender: UIButton!) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
    }
    
    @IBAction func exitPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
