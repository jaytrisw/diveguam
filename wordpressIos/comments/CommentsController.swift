//
//  CommentsController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/5/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CommentsController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout{
    
    // MARK: - IBOutlets
    @IBOutlet weak var noConnectionLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // MARK: - Arrays
    var commentsList = Array<ItemComment>()
    
    // MARK: - Variables
    var commentState:Bool = true
    var waiting:Bool = true
    var page:Int = 1
    var item_id:String = ""
    var post_url:String = ""
    var width:CGFloat = 0.0
    var commentCount:String = ""
    var web_id:String = ""
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
        putData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commentsList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CommentViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_comment", for: indexPath) as! CommentViewCell
        
        
        // put data into the cell
        let defaultImage:UIImage = UIImage(named:"placeholder")!
        let imageSrc = commentsList[indexPath.row].comment_avatar
        if(imageSrc == nil || imageSrc == ""){
            cell.avatar.image = defaultImage
        }else{
            cell.avatar.af_setImage(
                withURL: URL(string:imageSrc!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        cell.commentText.text = commentsList[indexPath.row].comment!
        cell.dateTxt.text = commentsList[indexPath.row].date!
        cell.usernameTxt.text = commentsList[indexPath.row].user_fullname!
        if (commentsList[indexPath.row].comment_professional_title != "error") {
        cell.proLabel.text = commentsList[indexPath.row].comment_professional_title! + " #" + commentsList[indexPath.row].comment_professional_number!
        } else {
            cell.proLabel.removeFromSuperview()
        }
        // adjustUITextViewHeight(arg: cell.commentText)
        // cell.sizeToFit()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        width = self.collectionView.bounds.size.width
        let attributedString = NSAttributedString(string: commentsList[indexPath.row].comment!, attributes: [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 18)!])
        
        let boundingRect = attributedString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        return CGSize(width: width, height: boundingRect.height+85)
    }
    
    func putData() {
        loadingIndicator.startAnimating()
        checkReachability()
        if collectionView != nil {
            self.collectionView.reloadData()
        }
        ApiNews.getComments(post_url: post_url) { (comments, success, statement) in
            if statement == "open"{
                self.commentState = true
            }
            for item in comments {
                let a = item.comment?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                let b = a?.replacingOccurrences(of: "&[^;]+;", with: "", options: String.CompareOptions.regularExpression, range: nil)
                item.comment = b
                let str = item.date!
                let result = String(str.characters.prefix(10))
                item.date = str
                self.commentsList.append(item)
                self.loadingIndicator.stopAnimating()
                self.collectionView.reloadData()
            }
            if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                if self.commentsList.count != 0 {
                    self.loadingIndicator.stopAnimating()
                } else {
                    self.loadingIndicator.stopAnimating()
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 21))
                    label.center = self.view.center
                    label.textAlignment = .center
                    label.textColor = UIColor.white
                    label.font = UIFont (name: "AvenirNext-Bold", size: 17)
                    label.text = "Be the first to comment!"
                    label.alpha = 0
                    self.view.addSubview(label)
                    label.fadeIn()
                }
            }
        }
    }
    
    func adjustUITextViewHeight(arg : UITextView) {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    // MARK: - Setup View Controller
    func setupVC() {
        noConnectionLabel.alpha = 0
        refreshButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        refreshButton.alpha = 0
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        if let number = Int(web_id.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
        }
    }
    
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            self.collectionView.reloadData()
            noConnectionLabel.text = "No internet connection."
            noConnectionLabel.alpha = 1
            refreshButton.alpha = 1
        }
    }
    
    // MARK: - Actions
    @objc func buttonAction(sender: UIButton!) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        putData()
    }
    @IBAction func cancelBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
}


