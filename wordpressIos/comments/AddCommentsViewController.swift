//
//  AddCommentViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/8/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class AddCommentsViewController: UIViewController, UITextFieldDelegate {

    @IBAction func saveComment(_ sender: Any) {
        self.addComment(comment: commentValue.text!, email: emailValue.text!, name: nameValue.text!)
        self.loadingBackground.fadeIn()
        self.loadingIndicator.startAnimating()
    }
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingBackground: UIVisualEffectView!
    @IBOutlet weak var commentContainer: UIVisualEffectView!
    @IBOutlet weak var commentValue: UITextField!
    @IBOutlet weak var nameContainer: UIVisualEffectView!
    @IBOutlet weak var nameValue: UITextField!
    @IBOutlet weak var emailContainer: UIVisualEffectView!
    @IBOutlet weak var emailValue: UITextField!
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var item_id:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.stopAnimating()
        loadingBackground.alpha = 0
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        self.commentValue.delegate = self
        // Do any additional setup after loading the view.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()  //if desired
        UserDefaults.standard.set(nameValue.text, forKey: "name")
        UserDefaults.standard.set(emailValue.text, forKey: "email")
        self.addComment(comment: commentValue.text!, email: emailValue.text!, name: nameValue.text!)
        self.loadingBackground.fadeIn()
        self.loadingIndicator.startAnimating()
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let name = UserDefaults.standard.object(forKey: "name") as? String {
            nameValue.text = name
        }
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            emailValue.text = email
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addComment(comment:String ,email:String ,name:String)  {
        ApiNews.addComment(comment: comment, email: email, name: name, item_id: item_id) { (success) in
            if success {
                // self.view.makeToast("Added")
                self.view.endEditing(true)
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }else{
                self.view.makeToast("Error")
                self.loadingBackground.alpha = 0
                self.loadingIndicator.stopAnimating()
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
