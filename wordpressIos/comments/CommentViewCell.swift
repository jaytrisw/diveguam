//
//  CommentViewCell.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/5/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CommentViewCell: UICollectionViewCell {
    
    @IBOutlet weak var proLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var usernameTxt: UILabel!
    @IBOutlet weak var dateTxt: UILabel!
}
