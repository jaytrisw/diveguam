//
//  FirstViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 12/21/17.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   
    @IBOutlet weak var flowlayout:UICollectionViewFlowLayout!
    var NewsList = Array<ItemNews>()
    @IBOutlet weak var CollectionViewList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        putData()
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim-1, height: 245)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "OpenItem", sender: NewsList[indexPath.row])
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OpenItemController {
            if let News=sender as? ItemNews {
                dest.SingleItem=News
            }
        }
    }
    func putData() {
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_one", for: indexPath) as! CollectionViewCell
        //cell.laytitle.text=NewsList[indexPath.row].title!
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                print("Portrait")
            case .landscapeLeft,.landscapeRight :
                print("Landscape")
            default:
                print("Anything But Portrait")
            }
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            let dim = self.view.frame.size.width
            self.flowlayout.itemSize = CGSize(width: dim-1, height: 245)
        })
        super.viewWillTransition(to: size, with: coordinator)
        
    }
}

