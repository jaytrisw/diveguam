//
//  ItemCat.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/4/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation
class ItemCat{
    var id:String?
    var title:String?
    var postsCount:String?
    var imageUrl:String?
    
    init(id:String,title:String,postsCount:String,imageUrl:String){
        self.title=title
        self.id=id
        self.postsCount=postsCount
        self.imageUrl=imageUrl
    }
}

