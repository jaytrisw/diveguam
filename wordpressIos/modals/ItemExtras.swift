//
//  ItemConditions.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/5/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation

class ItemExtras {
    var commentCount:String?
    var weather:String?
    var temperature:String?
    var beaufort:String?
    var windSpeed:String?
    var windDirection:String?
    var firstTideType:String?
    var firstTideHeight:String?
    var firstTideTime:String?
    var secondTideType:String?
    var secondTideHeight:String?
    var secondTideTime:String?
    var avatar:String?
    var author:String?
    var authorProRating:String?
    var authorProNumber:String?
    
    init(commentCount:String,weather:String,temperature:String,beaufort:String,windSpeed:String,windDirection:String,firstTideType:String,firstTideHeight:String,firstTideTime:String,secondTideType:String,secondTideHeight:String,secondTideTime:String,avatar:String,author:String,authorProRating:String,authorProNumber:String){
        self.commentCount=commentCount
        self.weather=weather
        self.temperature=temperature
        self.windSpeed=windSpeed
        self.beaufort=beaufort
        self.windDirection=windDirection
        self.firstTideType=firstTideType
        self.firstTideHeight=firstTideHeight
        self.firstTideTime=firstTideTime
        self.secondTideType=secondTideType
        self.secondTideHeight=secondTideHeight
        self.secondTideTime=secondTideTime
        self.avatar=avatar
        self.author=author
        self.authorProRating=authorProRating
        self.authorProNumber=authorProNumber
    }
}
