//
//  ExtraFields.swift
//  wordpressIos
//
//  Created by Joshua Wood on 10/29/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation

class extraFields {
    // Image Fields
    var imageLink:String?
    var imagePhotographer:String?
    var imagePhotographerLink:String?
    var imageUrl:String?
    var imageTitle:String?
    // Certification Fields
    var certId:String?
    var certTitle:String?
    var certDescription:String?
    var certImage:String?
    
    init(
        imageLink:String,
        imagePhotographer:String,
        imagePhotographerLink:String,
        imageUrl:String,
        imageTitle:String,
        certId:String,
        certTitle:String,
        certDescription:String,
        certImage:String
        ) {
        
        self.imageLink=imageLink
        self.imagePhotographer=imagePhotographer
        self.imagePhotographerLink=imagePhotographerLink
        self.imageUrl=imageUrl
        self.imageTitle=imageTitle
        self.certId=certId
        self.certTitle=certTitle
        self.certDescription=certDescription
        self.certImage=certImage
    }
}

class rawJSON {
    var json:Array<Any>?
    init(json:Array<Any>) {
        self.json=json
    }
}
