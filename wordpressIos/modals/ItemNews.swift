//
//  ItemNews.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 12/21/17.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation
class ItemNews{
    var id:String?
    var postID:String?
    var title:String?
    var imgUrl:String?
    var datetime:String?
    var writer:String?
    var newUrl:String?
    var newContent:String?
    var depthRange:String?
    var visRange:String?
    var latitude:String?
    var longitude:String?
    var city:String?
    var certificationLevel:String?
    var access:String?
    var hazards:String?
    var siteMap:String?
    var commentCount:String?
    var marineLife:String?
    var gallery:String?
    var warning:String?
    var warningContact:String?
    var warningLabel:String?
    var warningSubject:String?
    var warningInstructions:String?
    var firstAid:String?
    var firstAidTitle:String?
    var firstAidContent:String?
    var firstAidImageUrl:String?
    var firstAidSymptoms:String?
    var firstAidDescription:String?
    var avatar:String?
    var author:String?
    var authorProRating:String?
    var authorProNumber:String?
    var newWebId:String?
    var newSaved:Bool?
    init(id:String,postID:String,title:String,imgUrl:String,datetime:String,writer:String,newUrl:String,newContent:String,depthRange:String,visRange:String,latitude:String,longitude:String,city:String,certificationLevel:String,access:String,hazards:String,siteMap:String,commentCount:String,marineLife:String,gallery:String,warning:String,warningContact:String,warningLabel:String,warningSubject:String,warningInstructions:String,firstAid:String,firstAidTitle:String,firstAidContent:String,firstAidImageUrl:String,firstAidSymptoms:String,firstAidDescription:String,avatar:String,author:String,authorProRating:String,authorProNumber:String){
        self.title=title
        self.datetime=datetime
        self.writer=writer
        self.imgUrl=imgUrl
        self.id=id
        self.postID=postID
        self.newUrl=newUrl
        self.newContent=newContent
        self.depthRange=depthRange
        self.visRange=visRange
        self.latitude=latitude
        self.longitude=longitude
        self.city=city
        self.certificationLevel=certificationLevel
        self.access=access
        self.hazards=hazards
        self.siteMap=siteMap
        self.commentCount=commentCount
        self.marineLife=marineLife
        self.gallery=gallery
        self.warning=warning
        self.warningContact=warningContact
        self.warningLabel=warningLabel
        self.warningSubject=warningSubject
        self.warningInstructions=warningInstructions
        self.firstAid=firstAid
        self.firstAidTitle=firstAidTitle
        self.firstAidContent=firstAidContent
        self.firstAidImageUrl=firstAidImageUrl
        self.firstAidSymptoms=firstAidSymptoms
        self.firstAidDescription=firstAidDescription
        self.avatar=avatar
        self.author=author
        self.authorProRating=authorProRating
        self.authorProNumber=authorProNumber
    }
}
