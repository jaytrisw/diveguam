//
//  ProfileFields.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 6/6/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation

class ProfileFields {
    
    var avatar:String?
    var author:String?
    var authorProRating:String?
    var authorProNumber:String?
    var phoneNumber:String?
    var emailAddress:String?
    var url:String?
    var urlAlt:String?
    var specialtyRatings:String?
    var headerUrl:String?
    init(avatar:String,author:String,authorProRating:String,authorProNumber:String,phoneNumber:String,emailAddress:String,url:String,urlAlt:String,specialtyRatings:String,headerUrl:String){
        
        self.avatar=avatar
        self.author=author
        self.authorProRating=authorProRating
        self.authorProNumber=authorProNumber
        self.phoneNumber=phoneNumber
        self.emailAddress=emailAddress
        self.url=url
        self.urlAlt=urlAlt
        self.specialtyRatings=specialtyRatings
        self.headerUrl=headerUrl
        
    }
}
