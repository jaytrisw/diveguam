//
//  ItemBoats.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 6/12/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation

class ItemBoats {
    var company:String?
    var timestamp:String?
    
    init(company:String,timestamp:String){
        self.company=company
        self.timestamp=timestamp
    }
    
}
