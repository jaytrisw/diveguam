//
//  ItemComment.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/5/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation
class ItemComment {
    var comment:String?
    var date:String?
    var user:String?
    var user_fullname:String?
    var comment_id:String?
    var comment_avatar:String?
    var comment_professional_number:String?
    var comment_professional_title:String?
}

