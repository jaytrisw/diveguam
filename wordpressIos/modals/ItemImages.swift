//
//  ItemComment.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 4/5/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import Foundation

class ItemImages {
    var link:String?
    var photographer:String?
    var photographerLink:String?
    var imgUrl:String?
    var title:String?
    
    init(link:String,photographer:String,photographerLink:String,imgUrl:String,title:String){
        self.link=link
        self.photographer=photographer
        self.photographerLink=photographerLink
        self.imgUrl=imgUrl
        self.title=title
    }
}

class ItemCertifications {
    var id:String?
    var title:String?
    var description:String?
    var image:String?
    
    init(id:String,title:String,description:String,image:String){
        self.id=id
        self.title=title
        self.description=description
        self.image=image
    }
}
