//
//  MapViewCellViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 6/20/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import MapKit

class MapViewCellViewController: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    // MARK: - Arrays
    var NewsList = Array<ItemNews>()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        //13.456011, 144.773661
        let location = CLLocationCoordinate2D(latitude: 13.456011, longitude: 144.773661)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.4, longitudeDelta: 0.4)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        ApiNews.getNewsByCategory(page: 1, count: 75, id: "12", completion: { (data:Array<ItemNews>, success:Bool) in
            if success {
                for item in data {
                    self.NewsList.append(item)
                }
                //self.waiting = false
                var i = 0
                repeat {
                    let lat = Double(self.NewsList[i].latitude ?? "") ?? 0.0
                    let long = Double(self.NewsList[i].longitude ?? "") ?? 0.0
                    let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = location
                    annotation.title = self.NewsList[i].title
                    self.mapView.addAnnotations([annotation])
                    i = i + 1
                } while i < self.NewsList.count
                self.loadingIndicator.stopAnimating()
            } else {

            }
        })
        
        
        // Do any additional setup after loading the view.
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        print("Selected")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
