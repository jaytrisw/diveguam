//
//  PhotoViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 10/28/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import FloatingPanel

class PhotoViewController: UIViewController {    
    
    @IBOutlet weak var photographerLabel: UILabel!
    @IBOutlet weak var photoTitle: UILabel!
    @IBOutlet weak var smallImage: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    
    var imgList = Array<ItemImages>()
    var defaultImage: UIImage!
    var indexPath:Int!
    //var titleString:String!
    //var photographerString:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImage = UIImage(named:"placeholder")!
        photoTitle.text = imgList[indexPath].title
        photographerLabel.text = imgList[indexPath].photographer
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        imageView.image = nil
        let esc_str = imgList[indexPath].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if(imgList[indexPath].imgUrl == nil || imgList[indexPath].imgUrl == ""){
            imageView.image = defaultImage
            smallImage.image = defaultImage
        } else {
            imageView.af_setImage(withURL: URL(string:esc_str!)!, placeholderImage: defaultImage, imageTransition: .crossDissolve(0.2))
            smallImage.af_setImage(withURL: URL(string:esc_str!)!, placeholderImage: defaultImage, imageTransition: .crossDissolve(0.2))
        }
    }

    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
