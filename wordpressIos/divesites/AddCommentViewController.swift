//
//  AddCommentViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 10/28/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class AddCommentViewController: UIViewController, UITextFieldDelegate {
    
    var item_id:String!
    var id:Int!
    
    @IBOutlet weak var commentValue: UITextField!
    @IBOutlet weak var nameValue: UITextField!
    @IBOutlet weak var emailValue: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentValue.delegate = self
        if let myNumber = NumberFormatter().number(from: item_id) {
            id = myNumber.intValue
        }
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        commentValue.resignFirstResponder()  //if desired
        UserDefaults.standard.set(nameValue.text, forKey: "name")
        UserDefaults.standard.set(emailValue.text, forKey: "email")
        self.addComment(comment: commentValue.text!, email: emailValue.text!, name: nameValue.text!)
        //self.loadingBackground.fadeIn()
        //self.loadingIndicator.startAnimating()
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let name = UserDefaults.standard.object(forKey: "name") as? String {
            nameValue.text = name
        }
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            emailValue.text = email
        }
    }
    
    func addComment(comment:String ,email:String ,name:String)  {
        ApiNews.addComment(comment: comment, email: email, name: name, item_id: id!) { (success) in
            if success {
                // self.view.makeToast("Added")
                print("Success")
                print(self.id)
                self.view.endEditing(true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
