//
//  DiveSiteViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 10/24/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import FloatingPanel
import MapKit
import Toast_Swift
import Alamofire
import SwiftyJSON
import AlamofireImage
import GoogleMobileAds

class DiveSiteViewController: UIViewController, MKMapViewDelegate, FloatingPanelControllerDelegate {
    
    // MARK: - Arrays
    var SingleItem:ItemNews?
    
    // MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Variables
    var fpc: FloatingPanelController!
    var searchVC: SiteDetailViewController!
    var defaultImage:UIImage!
    var indexPath:Int!
    var certList = Array<ItemCertifications>()

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CertificationsViewController {
            dest.certList = certList
            dest.indexPath = indexPath
            dest.defaultImage = defaultImage
        }
    }
    
    func showCerts() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.performSegue(withIdentifier: "showCerts", sender: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImage = UIImage(named:"placeholder")!
        fpc = FloatingPanelController()
        fpc.delegate = self
        searchVC = storyboard?.instantiateViewController(withIdentifier: "SiteDetail") as? SiteDetailViewController
        // Initialize FloatingPanelController and add the view
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.cornerRadius = 20.0
        fpc.surfaceView.shadowHidden = false
        
        // Add a content view controller
        fpc.show(searchVC, sender: self)
        fpc.track(scrollView: searchVC.scrollView)

        setupMap()
        setupDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fpc.addPanel(toParent: self, animated: true)
        // Must be here
        // searchVC.searchBar.delegate = self
    }

    func setupDetails() {
        searchVC.SingleItem = SingleItem
        searchVC.post_url = SingleItem?.newUrl!
        searchVC.item_id = SingleItem?.id!
        
        let title = SingleItem?.title
        searchVC.siteTitle.text = title
        let subtitle = SingleItem?.city
        searchVC.siteSubtitle.text = subtitle
        let depth = SingleItem?.depthRange
        searchVC.depthValueTop.text = depth
        searchVC.depthValue.text = depth
        let visibility = SingleItem?.visRange
        searchVC.visibilityValue.text = visibility
        let access = SingleItem?.access
        searchVC.accessValue.text = access
        let content = SingleItem?.newContent
        searchVC.descriptionText.text = content
        searchVC.siteImage.af_cancelImageRequest()
        searchVC.siteImage.layer.removeAllAnimations()
        searchVC.siteImage.image = nil
        let esc_str = SingleItem?.imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if(SingleItem?.imgUrl == nil || SingleItem?.imgUrl == ""){
            searchVC.siteImage.image = defaultImage
        } else {
            searchVC.siteImage.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        }
        if (SingleItem?.siteMap != "") {
            let esc_str = SingleItem?.siteMap?
                .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            searchVC.diveSiteMap.af_setImage(
                withURL: URL(string:esc_str!)!,
                placeholderImage: defaultImage,
                imageTransition: .crossDissolve(0.2)
            )
        } else {
            searchVC.diveSiteMap.removeFromSuperview()
            let pinTop = NSLayoutConstraint(item: searchVC.descriptionSeparator, attribute: .top, relatedBy: .equal,
                                            toItem: searchVC.descriptionText, attribute: .bottom, multiplier: 1.0, constant: 15)
            searchVC.descriptionText.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
        if SingleItem?.marineLife != "" {
            let marineLife = SingleItem?.marineLife
            searchVC.marineLifeText.text = marineLife
        } else {
            searchVC.marineLifeLabel.removeFromSuperview()
            searchVC.marineLifeText.removeFromSuperview()
            
            if SingleItem?.hazards != "" {
                let pinTop = NSLayoutConstraint(item: searchVC.hazardsLabel, attribute: .top, relatedBy: .equal,
                                                toItem: searchVC.accessValue, attribute: .bottom, multiplier: 1.0, constant: 8)
                searchVC.accessValue.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([pinTop])
            }
            
            
        }
        if SingleItem?.hazards != "" {
            let hazards = SingleItem?.hazards
            searchVC.hazardsText.text = hazards
        } else {
            if SingleItem?.marineLife != "" {
                searchVC.hazardsLabel.removeFromSuperview()
                searchVC.hazardsText.removeFromSuperview()
                let pinTop = NSLayoutConstraint(item: searchVC.siteDetailsSeparator, attribute: .top, relatedBy: .equal,
                                                toItem: searchVC.marineLifeText, attribute: .bottom, multiplier: 1.0, constant: 15)
                searchVC.marineLifeText.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([pinTop])
            } else {
                searchVC.hazardsLabel.removeFromSuperview()
                searchVC.hazardsText.removeFromSuperview()
                let pinTop = NSLayoutConstraint(item: searchVC.siteDetailsSeparator, attribute: .top, relatedBy: .equal,
                                                toItem: searchVC.accessValue, attribute: .bottom, multiplier: 1.0, constant: 15)
                searchVC.accessValue.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([pinTop])
            }
        }
    }
    func setupMap() {
        let lat = Double(SingleItem?.latitude ?? "") ?? 0.0
        let long = Double(SingleItem?.longitude ?? "") ?? 0.0
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = SingleItem?.title
        mapView.addAnnotation(annotation)
        var center = location;
        center.latitude -= self.mapView.region.span.latitudeDelta / 6.0;
        mapView.setCenter(center, animated: true);
        //mapView.isZoomEnabled = false
        //mapView.isScrollEnabled = false
    }
    
    // MARK: FloatingPanelControllerDelegate
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        switch traitCollection.verticalSizeClass {
        case .compact:
            fpc.surfaceView.borderWidth = 1.0 / traitCollection.displayScale
            fpc.surfaceView.borderColor = UIColor.black.withAlphaComponent(0.2)
        default:
            fpc.surfaceView.borderWidth = 0.0
            fpc.surfaceView.borderColor = nil
        }
        return nil
    }
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        let y = vc.surfaceView.frame.origin.y
        let tipY = vc.originYOfSurface(for: .tip)
        if y > tipY - 44.0 {
            //let progress = max(0.0, min((tipY  - y) / 44.0, 1.0))
            //self.searchVC.tableView.alpha = progress
        }
    }
    func floatingPanelWillBeginDragging(_ vc: FloatingPanelController) {
        if vc.position == .full {
        }
    }
    func floatingPanelDidEndDragging(_ vc: FloatingPanelController, withVelocity velocity: CGPoint, targetPosition: FloatingPanelPosition) {
        if targetPosition != .full {
        }
        
        UIView.animate(withDuration: 0.25,
                       delay: 0.0,
                       options: .allowUserInteraction,
                       animations: {
                        if targetPosition == .tip {
                            //self.searchVC.tableView.alpha = 0.0
                        } else {
                            //self.searchVC.tableView.alpha = 1.0
                        }
        }, completion: nil)
    }
}
class SiteDetailViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate {
    
    // MARK: - Arrays
    var SingleItem:ItemNews?
    var extrasList = Array<ItemExtras>()
    var imgList = Array<ItemImages>()
    var certList = Array<ItemCertifications>()
    var boatList = Array<ItemBoats>()
    var commentsList = Array<ItemComment>()
    var rawJSON = Array<rawJSON>()
    
    // MARK: - IBOutlets
    @IBOutlet weak var suggestedCertTable: UITableView!
    @IBOutlet weak var commentsTableSeparator: UIView!
    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet weak var tidesStack: UIStackView!
    @IBOutlet weak var weatherStack: UIStackView!
    @IBOutlet weak var favoriteButtonLabel: CenteredButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var galleryIndicator: UIActivityIndicatorView!
    @IBOutlet weak var recommendedCertLabel: UILabel!
    @IBOutlet weak var recommendedCertSeparator: UIView!
    @IBOutlet weak var photoSeparator: UIView!
    @IBOutlet weak var adBlockSeparator: UIView!
    @IBOutlet weak var adBlock: UIView!
    @IBOutlet weak var siteDetailsSeparator: UIView!
    @IBOutlet weak var hazardsText: UITextView!
    @IBOutlet weak var hazardsLabel: UILabel!
    @IBOutlet weak var marineLifeText: UITextView!
    @IBOutlet weak var marineLifeLabel: UILabel!
    @IBOutlet weak var stackViewSeparator: UIView!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var descriptionSeparator: UIView!
    @IBOutlet weak var diveSiteMap: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionViewList: UICollectionView!
    @IBOutlet weak var siteTitle: UILabel!
    @IBOutlet weak var siteSubtitle: UILabel!
    @IBOutlet weak var siteImage: UIImageView!
    @IBOutlet weak var depthValue: UILabel!
    @IBOutlet weak var depthValueTop: UILabel!
    @IBOutlet weak var visibilityValue: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var accessValue: UILabel!
    @IBOutlet weak var bannerAd: GADBannerView!
    @IBOutlet weak var reportAction: UIButton!
    
    // MARK: - IBOutlets (Tidal Conditions)
    @IBOutlet weak var firstTideType: UIImageView!
    @IBOutlet weak var firstTide: UILabel!
    @IBOutlet weak var tideSep: UILabel!
    @IBOutlet weak var secondTideType: UIImageView!
    @IBOutlet weak var secondTide: UILabel!
    
    // MARK: - IBOutlets (Weather Conditions)
    @IBOutlet weak var weatherType: UIImageView!
    @IBOutlet weak var weatherValue: UILabel!
    @IBOutlet weak var fahrenheit: UIImageView!
    @IBOutlet weak var tempSep: UILabel!
    @IBOutlet weak var beaufort: UIImageView!
    @IBOutlet weak var windSpeedValue: UILabel!
    @IBOutlet weak var windDirectionImage: UIImageView!
    @IBOutlet weak var windDirection: UILabel!
    
    // MARK: - Variables
    var mapVC: DiveSiteViewController!
    var commentVC: AddCommentViewController!
    var photoVC: PhotoViewController!
    var certVC: CertificationsViewController!
    var post_url:String!
    var item_id:String!
    var waiting:Bool = true
    var indexPath:Int?
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? AddCommentViewController {
            dest.item_id = item_id
        }
        if let dest = segue.destination as? CertificationsViewController {
            dest.certList = certList
            dest.indexPath = indexPath
        }
        if let dest = segue.destination as? PhotoViewController {
            dest.imgList = imgList
            dest.indexPath = indexPath
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapVC = storyboard?.instantiateViewController(withIdentifier: "mapVC") as? DiveSiteViewController
        setupVC()
        addGoogleAd()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.galleryIndicator.startAnimating()
            self.getImages()
            self.getCertifications()
            self.getConditions()
            self.getComments()
        }
    }
    
    // MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.indexPath = indexPath.row
        performSegue(withIdentifier: "showPhoto", sender: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.imgList.count == 0) {
            print(self.imgList.count)
        }
        print(self.imgList.count)
        return imgList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SitePhotoCellCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photo_cell", for: indexPath) as! SitePhotoCellCollectionViewCell
        cell.galleryPhoto.af_cancelImageRequest()
        cell.galleryPhoto.layer.removeAllAnimations()
        cell.galleryPhoto.image = nil
        let esc_str = self.imgList[indexPath.row].imgUrl?
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            cell.galleryPhoto.image = mapVC.defaultImage
                cell.galleryPhoto.af_setImage(
                    withURL: URL(string:esc_str!)!,
                    placeholderImage: mapVC.defaultImage,
                    imageTransition: .crossDissolve(0.2)
            )
        return cell
        
    }
    
    //MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return certList.count
        } else if tableView == self.suggestedCertTable {
            return 1
        } else {
            return commentsList.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cert_cell", for: indexPath) as! CertificationTableViewCell
            cell.certificationLabel?.text = self.certList[indexPath.row].title
            return cell
        } else if tableView == self.suggestedCertTable {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cert_cell", for: indexPath) as! CertificationTableViewCell
            cell.certificationLabel?.text = "\(SingleItem?.certificationLevel ?? "Error") Diver"
            return cell
        } else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "comment_cell", for: indexPath) as! CommentCell
            let defaultImage:UIImage = UIImage(named:"placeholder")!
            let imageSrc = commentsList[indexPath.row].comment_avatar
            if(imageSrc == nil || imageSrc == ""){
                cell.avatar.image = defaultImage
            } else {
                cell.avatar.af_setImage(
                    withURL: URL(string:imageSrc!)!,
                    placeholderImage: defaultImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }
            cell.commentText.text = commentsList[indexPath.row].comment!
            cell.dateLabel.text = commentsList[indexPath.row].date!
            cell.nameLabel.text = commentsList[indexPath.row].user_fullname!
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.indexPath = indexPath.row
            self.performSegue(withIdentifier: "showCerts", sender: nil)
        }
    }
    
    // MARK: - Get JSON Content
    func getConditions() {
        ApiNews.getExtras(post_url: post_url!, completion: { (data:Array<ItemExtras>, success:Bool) in
            if success {
                for item in data {
                    self.extrasList.append(item)
                }
                self.waiting = false
                if self.extrasList[0].weather != "error" {
                    if self.extrasList[0].weather == "clear-night" {
                        self.weatherType.image = UIImage(named:"weather-clear-night")
                    } else if self.extrasList[0].weather == "light-rain" {
                        self.weatherType.image = UIImage(named:"weather-light-rain")
                    } else if self.extrasList[0].weather == "rain" {
                        self.weatherType.image = UIImage(named:"weather-rain")
                    } else if self.extrasList[0].weather == "cloudy" {
                        self.weatherType.image = UIImage(named:"weather-cloudy")
                    } else if self.extrasList[0].weather == "typhoon" {
                        self.weatherType.image = UIImage(named:"weather-typhoon")
                    } else if self.extrasList[0].weather == "thunder" {
                        self.weatherType.image = UIImage(named:"weather-thunder")
                    } else {
                        self.weatherType.image = UIImage(named:"weather-sunny")
                    }
                    self.weatherValue.text = self.extrasList[0].temperature
                    if self.extrasList[0].beaufort == "beaufort-1" {
                        self.beaufort.image = UIImage(named:"beaufort-1")
                    } else if self.extrasList[0].beaufort == "beaufort-2" {
                        self.beaufort.image = UIImage(named:"beaufort-2")
                    } else if self.extrasList[0].beaufort == "beaufort-3" {
                        self.beaufort.image = UIImage(named:"beaufort-3")
                    } else if self.extrasList[0].beaufort == "beaufort-4" {
                        self.beaufort.image = UIImage(named:"beaufort-4")
                    } else if self.extrasList[0].beaufort == "beaufort-5" {
                        self.beaufort.image = UIImage(named:"beaufort-5")
                    } else if self.extrasList[0].beaufort == "beaufort-6" {
                        self.beaufort.image = UIImage(named:"beaufort-6")
                    } else if self.extrasList[0].beaufort == "beaufort-7" {
                        self.beaufort.image = UIImage(named:"beaufort-7")
                    } else if self.extrasList[0].beaufort == "beaufort-8" {
                        self.beaufort.image = UIImage(named:"beaufort-8")
                    } else if self.extrasList[0].beaufort == "beaufort-9" {
                        self.beaufort.image = UIImage(named:"beaufort-9")
                    } else if self.extrasList[0].beaufort == "beaufort-10" {
                        self.beaufort.image = UIImage(named:"beaufort-10")
                    } else if self.extrasList[0].beaufort == "beaufort-11" {
                        self.beaufort.image = UIImage(named:"beaufort-11")
                    } else if self.extrasList[0].beaufort == "beaufort-12" {
                        self.beaufort.image = UIImage(named:"beaufort-12")
                    } else {
                        self.beaufort.image = UIImage(named:"beaufort-0")
                    }
                    self.windSpeedValue.text = self.extrasList[0].windSpeed! + " mph"
                    if self.extrasList[0].windDirection == "NE" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-east")
                    } else if self.extrasList[0].windDirection == "E" {
                        self.windDirectionImage.image = UIImage(named:"direction-east")
                    } else if self.extrasList[0].windDirection == "SE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-east")
                    } else if self.extrasList[0].windDirection == "S" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "SW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-west")
                    } else if self.extrasList[0].windDirection == "W" {
                        self.windDirectionImage.image = UIImage(named:"direction-west")
                    } else if self.extrasList[0].windDirection == "NW" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-west")
                    } else if self.extrasList[0].windDirection == "ENE" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-east")
                    } else if self.extrasList[0].windDirection == "ESE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-east")
                    } else if self.extrasList[0].windDirection == "SSE" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "SSW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south")
                    } else if self.extrasList[0].windDirection == "WSW" {
                        self.windDirectionImage.image = UIImage(named:"direction-south-west")
                    } else if self.extrasList[0].windDirection == "WNW" {
                        self.windDirectionImage.image = UIImage(named:"direction-north-west")
                    } else {
                        self.windDirectionImage.image = UIImage(named:"direction-north")
                    }
                    self.windDirection.text = self.extrasList[0].windDirection
                    self.weatherStack.fadeIn()
                } else {
                    //self.weatherIndicator.stopAnimating()
                    //self.weatherError.text = "Parsing error"
                    //self.weatherError.fadeIn()
                }
                if self.extrasList[0].firstTideType != "error" {
                    if self.extrasList[0].firstTideType == "High" {
                        self.firstTideType.image = UIImage(named:"tide-high")
                    } else {
                        self.firstTideType.image = UIImage(named:"tide-low")
                    }
                    self.firstTide.text = self.extrasList[0].firstTideHeight! + " " + self.extrasList[0].firstTideTime!
                    if self.extrasList[0].secondTideType == "High" {
                        self.secondTideType.image = UIImage(named:"tide-high")
                    } else {
                        self.secondTideType.image = UIImage(named:"tide-low")
                    }
                    self.secondTide.text = self.extrasList[0].secondTideHeight! + " " + self.extrasList[0].secondTideTime!
                    self.tidesStack.fadeIn()
                } else {
                    //self.tideError.text = "Parsing error"
                    //self.tideError.fadeIn()
                }
                //self.weatherIndicator.stopAnimating()
                //self.tidesIndicator.stopAnimating()
            } else {
                if self.currentReachabilityStatus == .reachableViaWiFi || self.currentReachabilityStatus == .reachableViaWWAN {
                    print("Failed to download JSON data")
                    //self.weatherIndicator.stopAnimating()
                    //self.weatherError.text = "Error parsing data"
                    //self.weatherError.fadeIn()
                } else {
                    //self.checkReachability()
                }
            }
        })
    }
    func getCertifications() {
        ApiNews.getCertifications(post_url: self.post_url, completion: { (data:Array<ItemCertifications>, success:Bool) in
            if success {
                for item in data {
                    self.certList.append(item)
                }
                self.tableView.reloadData()
                self.updateTableHeight()
                self.waiting = false
            } else {
                print("This is the error block")
            }
        })
    }
    func getImages() {
        ApiNews.getImages(post_url: self.post_url, completion: { (data:Array<ItemImages>, success:Bool) in
            if success {
                    for item in data {
                        self.imgList.append(item)
                    }
                    self.galleryIndicator.fadeOut()
                    self.galleryIndicator.stopAnimating()
                    self.collectionViewList.reloadData()
                    self.updateCollectionView()
                    self.waiting = false
                } else {
                    self.galleryIndicator.fadeOut()
                    self.galleryIndicator.stopAnimating()
                    self.updateCollectionView()
                }
            })
    }
    func getJSON() {
        ApiNews.getExtraFields(post_url: self.post_url, completion: { (data:Array<rawJSON>, success:Bool) in
            if success {
                for item in data {
                    self.rawJSON.append(item)
                }
                print(data)
            }
        })
    }
    
    func getComments() {
        ApiNews.getComments(post_url: post_url) { (comments, success, statement) in
            if success {
                for item in comments {
                    let a = item.comment?.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
                    let b = a?.replacingOccurrences(of: "&[^;]+;", with: "", options: String.CompareOptions.regularExpression, range: nil)
                    item.comment = b
                    let str = item.date!
                    let result = String(str.characters.prefix(10))
                    item.date = str
                    self.commentsList.append(item)
                    self.commentsTable.reloadData()
                }
                self.updateCommentsTable()
            }
        }
    }
    
    // MARK: - Misc Functions
    func setupVC() {
        tableViewHeight.constant = 0
        recommendedCertLabel.alpha = 0
        weatherStack.alpha = 0
        tidesStack.alpha = 0
        tableView.alpha = 0
    }
    func addGoogleAd() {
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerAd.adUnitID = "ca-app-pub-6121528204590445/7890176775"
        bannerAd.rootViewController = self
        bannerAd.delegate = self
        bannerAd.load(request)
    }
    func shareTxt() {
        let text = "\(siteTitle.text ?? "Error") - Guam Dive Guide\n\(post_url ?? "Error")"
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    func saveToDB() {
        let dbHandler = DBHandler()
        dbHandler.beforeStart()
        
        if dbHandler.insertNews(item: SingleItem!){
            self.view.makeToast("Dive site saved.")
        }else{
            self.view.makeToast("This site has already been saved.")
        }
    }
    func deleteFromDB() {
        let dbHandler = DBHandler()
        dbHandler.beforeStart()
        
        if dbHandler.deleteNews(item: SingleItem!){
            self.view.makeToast("Item Deleted successfully")
            NotificationCenter.default.post(name: NSNotification.Name("load"), object: nil)
            dismiss(animated: true, completion: nil)
        }else{
            self.view.makeToast("Sorry there is some error ")
        }
    }
    func updateCommentsTable() {
        if commentsList.count == 0 {
            self.commentsTable.removeFromSuperview()
            let pinTop = NSLayoutConstraint(item: self.commentsTableSeparator, attribute: .top, relatedBy: .equal,
                                            toItem: self.adBlockSeparator, attribute: .bottom, multiplier: 1.0, constant: -1)
            self.adBlockSeparator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
    }
    func updateTableHeight() {
        if certList.count == 0 {
            self.tableView.removeFromSuperview()
            self.recommendedCertLabel.removeFromSuperview()
            let pinTop = NSLayoutConstraint(item: self.recommendedCertSeparator, attribute: .top, relatedBy: .equal,
                                            toItem: self.suggestedCertTable, attribute: .bottom, multiplier: 1.0, constant: 15)
            self.suggestedCertTable.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        } else {
            tableViewHeight.constant = CGFloat(certList.count * 35)
            recommendedCertLabel.fadeIn()
            tableView.fadeIn()
        }
    }
    func updateCollectionView() {
        self.galleryIndicator.fadeOut()
        galleryIndicator.stopAnimating()
        if imgList.count == 0 {
            self.collectionViewList.removeFromSuperview()
            let pinTop = NSLayoutConstraint(item: self.photoSeparator, attribute: .top, relatedBy: .equal,
                                            toItem: self.stackViewSeparator, attribute: .bottom, multiplier: 1.0, constant: -1)
            self.stackViewSeparator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
    }
    
    // MARK: - IBActions
    @IBAction func reportIssueButton(_ sender: Any) {
        print("Report Issue Button Pressed")
        
    }
    @IBAction func shareButton(_ sender: Any) {
        shareTxt()
    }
    @IBAction func favoriteButton(_ sender: Any) {
        if (SingleItem?.newSaved)! {
            self.deleteFromDB()
        } else {
            self.saveToDB()
        }
    }
    @IBAction func commentsButton(_ sender: Any) {
        self.performSegue(withIdentifier: "showComments", sender: nil)
    }
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
class SitePhotoCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var galleryPhoto: UIImageView!
    
}
class CertificationTableViewCell: UITableViewCell {
    @IBOutlet weak var certificationLabel: UILabel!
}
class CommentCell: UITableViewCell {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentText: UILabel!
}
class CenteredButton: UIButton {
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.titleRect(forContentRect: contentRect)
        
        return CGRect(x: 0, y: contentRect.height - rect.height + -5,
                      width: contentRect.width, height: rect.height)
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.imageRect(forContentRect: contentRect)
        let titleRect = self.titleRect(forContentRect: contentRect)
        
        return CGRect(x: contentRect.width/2.0 - rect.width/2.0,
                      y: (contentRect.height - titleRect.height)/2.0 - rect.height/2.0,
                      width: rect.width, height: rect.height)
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        
        if let image = imageView?.image {
            var labelHeight: CGFloat = 0.0
            
            if let size = titleLabel?.sizeThatFits(CGSize(width: self.contentRect(forBounds: self.bounds).width, height: CGFloat.greatestFiniteMagnitude)) {
                labelHeight = size.height
            }
            
            return CGSize(width: size.width, height: image.size.height + labelHeight + 5)
        }
        
        return size
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        centerTitleLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        centerTitleLabel()
    }
    
    private func centerTitleLabel() {
        self.titleLabel?.textAlignment = .center
    }
}
class SelfSizedTableView: UITableView {
    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }
}
class IntrinsicTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}
class FloatingPanelStocksLayout: FloatingPanelLayout {
    public var supportedPositions: [FloatingPanelPosition] {
        return [.full, .tip]
    }
    
    var initialPosition: FloatingPanelPosition {
        return .full
    }
    
    var topInteractionBuffer: CGFloat { return 0.0 }
    var bottomInteractionBuffer: CGFloat { return 0.0 }
    
    func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .full: return 26.0
        case .half: return 69.0
        case .tip: return 69.0 // Visible + ToolView
        }
    }
    
    func prepareLayout(surfaceView: UIView, in view: UIView) -> [NSLayoutConstraint] {
        return [
            surfaceView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0.0),
            surfaceView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0.0),
        ]
    }
    
    var backdropAlpha: CGFloat = 0.0
}
