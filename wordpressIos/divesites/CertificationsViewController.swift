//
//  CertificationsViewController.swift
//  wordpressIos
//
//  Created by Joshua Wood on 10/29/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class CertificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var defaultImage: UIImage!
    var indexPath:Int!
    var certList = Array<ItemCertifications>()
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var certTitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var minimumAgeTable: UITableView!
    
    @IBOutlet weak var instructorsTable: UITableView!
    @IBOutlet weak var conEdTable: UITableView!
    @IBOutlet weak var prerequisiteTable: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cert_cell", for: indexPath) as! CertificationTableViewCell
        cell.certificationLabel?.text = "Open Water Diver"
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImage = UIImage(named:"placeholder")!
        certTitle.text = certList[indexPath].title
        topTitle.text = certList[indexPath].title
        descriptionLabel.text = certList[indexPath].description
        image.af_cancelImageRequest()
        image.layer.removeAllAnimations()
        image.image = nil
        let esc_str = certList[indexPath].image?
                .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if(certList[indexPath].image == nil || certList[indexPath].image == ""){
            image.image = defaultImage
            topImage.image = defaultImage
        } else {
            image.af_setImage(withURL: URL(string:esc_str!)!, placeholderImage: defaultImage, imageTransition: .crossDissolve(0.2))
            topImage.af_setImage(withURL: URL(string:esc_str!)!, placeholderImage: defaultImage, imageTransition: .crossDissolve(0.2))
        }
    }

}
