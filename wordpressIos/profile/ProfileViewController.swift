//
//  ProfileViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/28/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import MessageUI

class ProfileViewController: UIViewController, UIScrollViewDelegate, MFMailComposeViewControllerDelegate {
    
    // MARK: - Interface Builder Outlets
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var imageBar: UIImageView!
    @IBOutlet weak var avatarLeading: NSLayoutConstraint!
    @IBOutlet weak var headerTop: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var noConnectionLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var specialtyRatingsLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var emailAddress: UIButton!
    @IBOutlet weak var specialtyRatings: UITextView!
    @IBOutlet weak var phoneNumber: UIButton!
    @IBOutlet weak var urlAlt: UIButton!
    @IBOutlet weak var url: UIButton!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var proRatingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var header: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Arrays
    var profileFields = Array<ProfileFields>()
    var Extras:ItemExtras?
    
    // MARK: - Variables
    var defaultAvatar:UIImage!
    var defaultImage:UIImage!
    var post_url:String = ""
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
        APICall()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup View Controller
    func setupVC() {
        scrollView.delegate = self
        checkReachability()
        modalPresentationCapturesStatusBarAppearance = true
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        defaultImage = UIImage(named:"placeholder")!
        defaultAvatar = UIImage(named:"user-placeholder")!
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        nameLabel.alpha = 0
        proRatingLabel.alpha = 0
        avatar.alpha = 0
        emailAddress.alpha = 0
        specialtyRatings.alpha = 0
        phoneNumber.alpha = 0
        urlAlt.alpha = 0
        url.alpha = 0
        headerImage.alpha = 0
        websiteLabel.alpha = 0
        phoneNumberLabel.alpha = 0
        emailLabel.alpha = 0
        specialtyRatingsLabel.alpha = 0
        closeButton.alpha = 0
        ratingLabel.alpha = 0
        imageBar.alpha = 0
    }
    
    // MARK: - API Call
    func APICall() {
        loadingIndicator.startAnimating()
        let post_url = self.post_url
        ApiNews.getProfile(post_url: post_url, completion: { (data:Array<ProfileFields>, success:Bool) in
            if success {
                print ("Success")
                for item in data {
                    self.profileFields.append(item)
                }
                if self.profileFields[0].author != "error" {
                    self.nameLabel.text = self.profileFields[0].author
                    self.nameLabel.fadeIn()
                }
                if self.profileFields[0].authorProRating != "error" {
                    self.proRatingLabel.text = self.profileFields[0].authorProRating! + " #" + self.profileFields[0].authorProNumber!
                    self.proRatingLabel.fadeIn()
                }
                if self.profileFields[0].avatar != "error" {
                    let esc_str = self.profileFields[0].avatar?
                    .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                    self.avatar.af_setImage(
                    withURL: URL(string:esc_str!)!,
                    placeholderImage: self.defaultAvatar,
                    imageTransition: .crossDissolve(0.2)
                )
                    self.avatar.fadeIn()
                }
                if self.profileFields[0].headerUrl != "error" {
                    let esc_str = self.profileFields[0].headerUrl?
                        .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                    self.headerImage.af_setImage(
                        withURL: URL(string:esc_str!)!,
                        placeholderImage: self.defaultImage,
                        imageTransition: .crossDissolve(0.2)
                    )
                    self.headerImage.fadeIn()
                }
                if self.profileFields[0].url != "error" {
                    self.url.setTitle(self.profileFields[0].url, for: .normal)
                    self.url.fadeIn()
                    self.websiteLabel.fadeIn()
                }
                if self.profileFields[0].url == "error" || self.profileFields[0].url == "" {
                    self.url.removeFromSuperview()
                    self.urlAlt.removeFromSuperview()
                    self.websiteLabel.removeFromSuperview()
                }
                if self.profileFields[0].urlAlt != "error" {
                    self.urlAlt.setTitle(self.profileFields[0].urlAlt, for: .normal)
                    self.urlAlt.fadeIn()
                }
                if self.profileFields[0].urlAlt == "" {
                    self.stackView.removeArrangedSubview(self.urlAlt)
                    self.urlAlt.removeFromSuperview()
                }
                if self.profileFields[0].phoneNumber != "error" {
                    self.phoneNumber.setTitle(self.profileFields[0].phoneNumber, for: .normal)
                    self.phoneNumber.fadeIn()
                    self.phoneNumberLabel.fadeIn()
                }
                if self.profileFields[0].phoneNumber == "error" || self.profileFields[0].phoneNumber == "" {
                    self.phoneNumber.removeFromSuperview()
                    self.phoneNumberLabel.removeFromSuperview()
                }
                if self.profileFields[0].emailAddress != "error" {
                    self.emailAddress.setTitle(self.profileFields[0].emailAddress, for: .normal)
                    self.emailAddress.fadeIn()
                    self.emailLabel.fadeIn()
                }
                if self.profileFields[0].specialtyRatings != "error" {
                    self.specialtyRatings.text = self.profileFields[0].specialtyRatings
                    self.specialtyRatings.fadeIn()
                    self.specialtyRatingsLabel.fadeIn()
                }
                if self.profileFields[0].specialtyRatings == "error" || self.profileFields[0].specialtyRatings == "" {
                    self.specialtyRatings.removeFromSuperview()
                    self.specialtyRatingsLabel.removeFromSuperview()
                }
                self.closeButton.fadeIn()
                self.ratingLabel.fadeIn()
                self.imageBar.fadeIn()
                self.loadingIndicator.stopAnimating()
            } else {
                print ("Failure")
                self.checkReachability()
            }
        })
        
    }
    
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / headerImage.bounds.height
            let headerSizevariation = ((headerImage.bounds.height * (1.0 + headerScaleFactor)) - headerImage.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            //headerImage.layer.transform = headerTransform
            
        } else {
            
            // Header -----------
            // headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_ImageStop, -offset), 0)
        }
        
        // Apply Transformations
        
        //headerImage.layer.transform = headerTransform
    }
    
    // MARK: - Reachability
    func checkReachability() {
        if currentReachabilityStatus == .reachableViaWiFi || currentReachabilityStatus == .reachableViaWWAN {
        } else {
            print("There is no internet connection")
            loadingIndicator.stopAnimating()
            noConnectionLabel.text = "No internet connection."
            //noConnectionLabel.alpha = 1
            //refreshButton.alpha = 1
            //closeButton.alpha = 1
        }
    }
    
    // MARK: - Mail Composer
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // MARK: - Actions
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func emailButton(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailAddress.currentTitle!])
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    @IBAction func phoneButton(_ sender: Any) {
        let unsafeChars = CharacterSet.alphanumerics.inverted  // Remove the .inverted to get the opposite result.
        let number = phoneNumber.currentTitle
        let cleanChars  = number?.components(separatedBy: unsafeChars).joined(separator: "")
        guard let num = URL(string: "tel://" + cleanChars!) else { return }
        UIApplication.shared.open(num)
    }
    @IBAction func urlAltButton(_ sender: Any) {
        if let url = URL(string: urlAlt.currentTitle!) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func urlButton(_ sender: Any) {
        if let url = URL(string: url.currentTitle!) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func refreshAction(_ sender: Any) {
        noConnectionLabel.alpha = 0
        refreshButton.alpha = 0
        APICall()
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
}
