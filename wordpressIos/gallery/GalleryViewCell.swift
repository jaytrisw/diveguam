//
//  GalleryViewCell.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/15/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit

class GalleryViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
}
