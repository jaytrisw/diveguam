//
//  GalleryViewController.swift
//  Guam Dive Guide
//
//  Created by Joshua Wood on 5/4/18.
//  Copyright © 2018 Joshua T. Wood. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage


class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    var imagesList = Array<ItemImages>()
    var post_url:String! = ""
    var defaultImage:UIImage!
    
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var CollectionViewList: UICollectionView!
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        let dim = self.view.frame.size.width
        flowlayout.itemSize = CGSize(width: dim / 3, height: dim / 3)
        defaultImage = UIImage(named:"placeholder")!
        putData()
    }
    
    func putData() {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:GalleryViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gallery_cell", for: indexPath) as! GalleryViewCell
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

